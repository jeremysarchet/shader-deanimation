/*
 * Derived from:
 *
 * "Hello GL"
 * http://duriansoftware.com/joe/An-intro-to-modern-OpenGL.-Table-of-Contents.html
 * https://github.com/jckarter/hello-gl
 *
 * I've been able to compile it with
 * `gcc sequence_blend.c include/glad.c -lGLFW -lIL -framework Cocoa -framework OpenGL -framework IOKit -o deanimate`
 *
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <IL/il.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define MAX_FILENAME_LENGTH 255
#define MAX_STRING_ARG_LENGTH 255
#define WIDTH 1920
#define HEIGHT 1080

int verbose = 0;

unsigned int load_image(const char *filename) {
    // Load an image at `filename` and return its DevIL id.
    ILuint image_id;
    // printf("Loading image: %s\n", filename);
    ilGenImages(1, &image_id);
    ilBindImage(image_id);
    ilEnable(IL_ORIGIN_SET);
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
    if (!ilLoadImage(filename)) {
      fprintf(stderr, "Couldn't load image: %s\n", filename);
      return 1;
    }
    // QUESTION: Does this do unnecessary work if the image is already RGB?
    ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
    // printf("Width: %d, Height %d, Bytes per Pixel %d\n", ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL));
    return image_id;
}

void write_image(char *filepath, int width, int height, void *buffer) {
    // Save an image to disk at `filepath` given image data in `buffer`.
    ILuint image;
    int pixel_size = 1 * 3; // 3 bytes for RGB
    ilGenImages(1, &image);
    ilBindImage(image);
    ilTexImage(width, height, 0, pixel_size, IL_RGB, IL_UNSIGNED_BYTE, buffer);
    ilSaveImage(filepath);
    ilDeleteImage(image);
}

void capture_image(GLFWwindow* window, char *filename) {
    // Save the framebuffer
    int fb_width, fb_height;
    glfwGetFramebufferSize(window, &fb_width, &fb_height);
    // printf("Framebuffer width: %d, height: %d\n", fb_width, fb_height);
    // Create a buffer to store the image data
    void *output;
    output = malloc(fb_width * fb_height * 3);
    // printf("Saving image with width: %d, height: %d\n", fb_width, fb_height);
    glReadPixels(0, 0, fb_width, fb_height, GL_RGB, GL_UNSIGNED_BYTE, output);
    // Write the image data with DevIL
    write_image(filename, fb_width, fb_height, output);
}

static void key_callback(GLFWwindow* window, int key, int scancode,
    int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

static void error_callback(int error, const char* description) {
    fputs(description, stderr);
}

void *file_contents(const char *filename, GLint *length) {
    FILE *f = fopen(filename, "r");
    void *buffer;

    if (!f) {
        fprintf(stderr, "Unable to open %s for reading\n", filename);
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);

    buffer = malloc(*length+1);
    *length = fread(buffer, 1, *length, f);
    fclose(f);
    ((char*)buffer)[*length] = '\0';

    return buffer;
}

// Global data for our program
static struct {
    GLuint vertex_buffer, element_buffer;
    GLuint textures[2];
    GLuint vertex_shader;

    GLuint framebuffer_a, framebuffer_b, framebuffer_c;
    GLuint colorbuffer_a, colorbuffer_b, colorbuffer_c;

    struct {
        GLuint blend_two_textures, show_texture;
    } fragment_shaders;

    struct {
        GLuint blend_two_textures, show_texture;
    } programs;

    struct {
        GLint textures[3];
    } uniforms;

    struct {
        GLint position;
    } attributes;
} resources;

static GLuint make_buffer(GLenum target, const void *buffer_data,
    GLsizei buffer_size) {
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(target, buffer);
    glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);
    return buffer;
}

void set_texture_parameters() {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
}

static GLuint make_texture(const char *filename) {
    int width, height;
    unsigned char* data;
    ILuint image_id;
    GLuint texture;

    // Load image with DevIL and get a handle to its data
    image_id = load_image(filename);
    ilBindImage(image_id);
    width = ilGetInteger(IL_IMAGE_WIDTH);
    height = ilGetInteger(IL_IMAGE_HEIGHT);
    data = ilGetData();

    // Make a texture with the image data
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    set_texture_parameters();
    glTexImage2D(
        GL_TEXTURE_2D, 0,           /* target, level */
        GL_RGB8,                    /* internal format */
        width, height, 0,           /* width, height, border */
        GL_RGB, GL_UNSIGNED_BYTE,   /* external format, type */
        data                        /* pixels */
    );
    // Delete the DevIL image and return the texture
    ilDeleteImage(image_id);
    return texture;
}

static GLuint make_colorbuffer() {
    GLuint colorbuffer;
    glGenTextures(1, &colorbuffer);
    glBindTexture(GL_TEXTURE_2D, colorbuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, WIDTH, HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    set_texture_parameters();
    return colorbuffer;
}

static GLuint make_framebuffer(GLuint colorbuffer) {
    GLuint framebuffer;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorbuffer, 0);

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        fprintf(stderr, "Trouble with framebuffer\n");
        return 1;
    }

    return framebuffer;
}

static void show_info_log(GLuint object, PFNGLGETSHADERIVPROC glGet__iv,
    PFNGLGETSHADERINFOLOGPROC glGet__InfoLog) {
    GLint log_length;
    char *log;

    glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);
    log = malloc(log_length);
    glGet__InfoLog(object, log_length, NULL, log);
    fprintf(stderr, "%s", log);
    free(log);
}

static GLuint make_shader(GLenum type, const char *filename) {
    GLint length;
    GLchar *source = file_contents(filename, &length);
    GLuint shader;
    GLint shader_ok;

    if (!source)
        return 0;

    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)&source, &length);
    free(source);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        fprintf(stderr, "Failed to compile %s:\n", filename);
        show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}

static GLuint make_program(GLuint vertex_shader, GLuint fragment_shader) {
    GLint program_ok;
    GLuint program = glCreateProgram();

    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        fprintf(stderr, "Failed to link shader program:\n");
        show_info_log(program, glGetProgramiv, glGetProgramInfoLog);
        glDeleteProgram(program);
        return 0;
    }
    return program;
}

// Data used to seed our vertex array and element array buffers:
static const GLfloat g_vertex_buffer_data[] = {
    -1.0f, -1.0f,
     1.0f, -1.0f,
    -1.0f,  1.0f,
     1.0f,  1.0f
};
static const GLushort g_element_buffer_data[] = { 0, 1, 2, 3 };

static int make_resources(const char* blend) {
    // Load and create all of our resources:
    resources.vertex_buffer = make_buffer(
        GL_ARRAY_BUFFER,
        g_vertex_buffer_data,
        sizeof(g_vertex_buffer_data)
    );
    resources.element_buffer = make_buffer(
        GL_ELEMENT_ARRAY_BUFFER,
        g_element_buffer_data,
        sizeof(g_element_buffer_data)
    );

    resources.vertex_shader = make_shader(GL_VERTEX_SHADER, "blend.vert");
    if (resources.vertex_shader == 0)
        return 0;

    resources.fragment_shaders.blend_two_textures = make_shader(GL_FRAGMENT_SHADER, blend);
    if (resources.fragment_shaders.blend_two_textures == 0)
        return 0;

    resources.fragment_shaders.show_texture = make_shader(GL_FRAGMENT_SHADER, "blends/show_texture.frag");
    if (resources.fragment_shaders.show_texture == 0)
        return 0;

    resources.programs.blend_two_textures = make_program(
        resources.vertex_shader,
        resources.fragment_shaders.blend_two_textures
    );
    if (resources.programs.blend_two_textures == 0)
        return 0;

    resources.programs.show_texture = make_program(
        resources.vertex_shader,
        resources.fragment_shaders.show_texture
    );
    if (resources.programs.show_texture == 0)
        return 0;

    resources.uniforms.textures[0]
        = glGetUniformLocation(resources.programs.blend_two_textures, "textures[0]");
    resources.uniforms.textures[1]
        = glGetUniformLocation(resources.programs.blend_two_textures, "textures[1]");
    resources.uniforms.textures[2]
        = glGetUniformLocation(resources.programs.blend_two_textures, "textures[2]");
    resources.attributes.position
        = glGetAttribLocation(resources.programs.blend_two_textures, "position");

    resources.colorbuffer_a = make_colorbuffer();
    resources.framebuffer_a = make_framebuffer(resources.colorbuffer_a);
    resources.colorbuffer_b = make_colorbuffer();
    resources.framebuffer_b = make_framebuffer(resources.colorbuffer_b);
    resources.colorbuffer_c = make_colorbuffer();
    resources.framebuffer_c = make_framebuffer(resources.colorbuffer_c);

    return 1;
}

void prepare_quad() {
    glBindBuffer(GL_ARRAY_BUFFER, resources.vertex_buffer);
    glVertexAttribPointer(
        resources.attributes.position,  /* attribute */
        2,                                /* size */
        GL_FLOAT,                         /* type */
        GL_FALSE,                         /* normalized? */
        sizeof(GLfloat)*2,                /* stride */
        (void*)0                          /* array buffer offset */
    );
    glEnableVertexAttribArray(resources.attributes.position);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resources.element_buffer);
}

void draw() {
    glDrawElements(
        GL_TRIANGLE_STRIP,  /* mode */
        4,                  /* count */
        GL_UNSIGNED_SHORT,  /* type */
        (void*)0            /* element array buffer offset */
    );
}

void render_filename(char *buffer, const char *path, int index, char *extension) {
    snprintf(buffer, MAX_FILENAME_LENGTH, "%s/%d.%s", path, index, extension);
    buffer[MAX_FILENAME_LENGTH] = '\0';
}

int blend_single(
        GLFWwindow* window,
        const char* input_dir,
        const char* output_dir,
        const char* output_name,
        int frame_count,
        const char* blend_mode
    ) {
    printf("Single...\n");

    int i;
    char filename[MAX_FILENAME_LENGTH + 1];
    GLuint zeroth_texture, leader_texture;

    // Load the zeroth texture
    render_filename(filename, input_dir, 0, "bmp");
    if (verbose) {
        printf("Zeroth:    %s\n", filename);
    }
    zeroth_texture = make_texture(filename);

    // Iterate through the frames
    for (i = 1; i < frame_count; ++i) {

        // Make the leader texture
        render_filename(filename, input_dir, i, "bmp");
        if (verbose) {
            printf("Leader:    %s\n", filename);
        }
        leader_texture = make_texture(filename);

        // First, blend the first two tail textures into A
        if (i == 1) {
            if (verbose) {
                printf("Blending:    0 and 1 into A\n");
            }
            glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_a);
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, zeroth_texture);
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, leader_texture);
            draw();
            continue;
        }
        // Then, blend subsequent tail textures via alternating buffers
        glActiveTexture(GL_TEXTURE0);
        if (i % 2 == 0) {
            if (verbose) {
                printf("Blending:    A and %d into B\n", i);
            }
            glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_b);
            glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_a);
        } else {
            if (verbose) {
                printf("Blending:    B and %d into A\n", i);
            }
            glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_a);
            glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_b);
        }
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, leader_texture);
        draw();
        if (verbose) {
            printf("\n");
        }
    }

    // Capture
    snprintf(filename, MAX_FILENAME_LENGTH, "%s/%s.jpg", output_dir, output_name);
    filename[MAX_FILENAME_LENGTH] = '\0';
    remove(filename);
    printf("Processed: %s\n\n", filename);
    capture_image(window, filename);

    return 0;
}

int blend_tail_sequence(
        GLFWwindow* window,
        const char* input_dir,
        const char* output_dir,
        int frame_count,
        const char* blend_mode,
        int step,
        int impression_count,
        const char* reference_image_filename
    ) {
    printf("Tail sequence...\n");

    int span = impression_count * step;
    // Ensure enough frames
    if (frame_count <= span) {
        fprintf(stderr, "Frame count must be greater than %d.\n", span);
        return 1;
    }
    printf("Span = %d * %d = %d\n", step, impression_count, span);
    printf("\n");

    int i, j;
    char filename[MAX_FILENAME_LENGTH + 1];
    GLuint leader_texture, follower_texture;
    int tail_cursor = 0;

    GLuint tail_textures[impression_count];
    GLuint reference_texture;

    // Load and setup the reference image if need be
    if (strncmp(blend_mode, "further-from", MAX_STRING_ARG_LENGTH) == 0){
        printf("Loading reference image: %s\n\n", reference_image_filename);
        reference_texture = make_texture(reference_image_filename);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, reference_texture);
    }

    // Populate the tail
    for (i = 0; i < impression_count; ++i) {
        render_filename(filename, input_dir, i * step, "bmp");
        printf("Loading impression: %s\n", filename);
        tail_textures[i] = make_texture(filename);
    }
    printf("\n");

    // Iterate through the sequence
    for (i = span; i < frame_count; ++i) {

        // Make the leader and follower textures
        render_filename(filename, input_dir, i, "bmp");
        if (verbose) {
            printf("Leader:    %s\n", filename);
        }
        leader_texture = make_texture(filename);
        render_filename(filename, input_dir, i - span, "bmp");
        printf("Follower:  %s\n", filename);
        follower_texture = make_texture(filename);

        // If need be, blend up the tail anew
        if (i % step == 0) {
            // Delete the oldest, replace it with the newest
            glDeleteTextures(1, &tail_textures[tail_cursor]);
            render_filename(filename, input_dir, i, "bmp");
            printf("Enqueued:  %s\n", filename);
            tail_textures[tail_cursor] = make_texture(filename);
            // Increment the cursor
            tail_cursor = (tail_cursor + 1) % impression_count;

            // Go through the tail in the order of the circular buffer
            j = 0;
            int circular_index;
            int zeroth;
            while (j < impression_count) {
                circular_index = (j + tail_cursor) % impression_count;
                if (j == 0) {
                    zeroth = circular_index;
                }
                if (j == 1) {
                    // First, blend the first two tail textures into A
                    if (verbose) {
                       printf("Blending:    0 and 1 into A\n");
                    }
                    glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_a);
                    glActiveTexture(GL_TEXTURE0);
                    glBindTexture(GL_TEXTURE_2D, tail_textures[zeroth]);
                    glActiveTexture(GL_TEXTURE1);
                    glBindTexture(GL_TEXTURE_2D, tail_textures[circular_index]);
                    glActiveTexture(GL_TEXTURE2);
                    glBindTexture(GL_TEXTURE_2D, reference_texture);
                    draw();
                    ++j;
                    continue;
                }
                // Then, blend subsequent tail textures via alternating buffers
                glActiveTexture(GL_TEXTURE0);
                if (j % 2 == 0) {
                    if (verbose) {
                        printf("Blending:    A and %d into B\n", j);
                    }
                    glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_b);
                    glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_a);
                } else {
                    if (verbose) {
                        printf("Blending:    B and %d into A\n", j);
                    }
                    glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_a);
                    glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_b);
                }
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, tail_textures[circular_index]);
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_2D, reference_texture);
                draw();

                ++j;
            }
        }

        // Blend the follwer and tail into C
        glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_c);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, follower_texture);
        glActiveTexture(GL_TEXTURE1);
        if (impression_count % 2 == 0) {
            // Completed tail is in A
            glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_a);
            if (verbose) {
                printf("Blending:  follower and A into C\n");
            }
        } else {
            // Completed tail in B
            glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_b);
            if (verbose) {
               printf("Blending:  follower and B into C\n");
            }
        }
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, reference_texture);
        draw();

        // Blend C and leader into default framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        if (verbose) {
           printf("Blending:  C and leader into default framebuffer\n");
        }
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_c);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, leader_texture);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, reference_texture);
        draw();

        // Delete the leader and follower textures
        glDeleteTextures(1, &leader_texture);
        glDeleteTextures(1, &follower_texture);

        // Capture
        render_filename(filename, output_dir, i, "bmp");
        remove(filename);
        printf("Processed: %s\n\n", filename);
        capture_image(window, filename);
    }

    // Cleanup the tail
    for (i = 0; i < impression_count; ++i) {
        glDeleteTextures(1, &tail_textures[i]);
    }

    // Cleanup the reference texture
    glDeleteTextures(1, &reference_texture);

    // Cleanup
    glDisableVertexAttribArray(resources.attributes.position);
    glDeleteFramebuffers(1, &resources.framebuffer_a);
    glDeleteFramebuffers(1, &resources.framebuffer_b);

    return 0;
}

int main(int argc, char** argv) {
    const char* input_dir;
    const char* output_dir;
    int frame_count;
    const char* program_mode;
    const char* blend_mode;
    int step = 1;
    int impression_count = 10;
    const char* reference_image_filename = NULL;
    const char* output_name;

    int got_input_dir = 0;
    int got_output_dir = 0;
    int got_frame_count = 0;
    int got_program_mode = 0;
    int got_blend_mode = 0;
    int got_reference_image_filename = 0;
    int flag;
    int index;

    // https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
    while ((flag = getopt (argc, argv, "i:o:l:m:b:s:t:p:f:vh")) != -1) {
        switch (flag) {
            case 'i':
                input_dir = optarg;
                /* TODO: Consider switching to the following
                 *
                 * char input_dir[MAX_FILENAME_LENGTH + 1];
                 * ...
                 * strncpy(input_dir, optarg, MAX_FILENAME_LENGTH);
                 *
                 * input_dir[MAX_FILENAME_LENGTH] = '\0';
                 */
                got_input_dir = 1;
                break;
            case 'o':
                output_dir = optarg;
                got_output_dir = 1;
                break;
            case 'l':
                frame_count = atoi(optarg);
                got_frame_count = 1;
                break;
            case 'm':
                program_mode = optarg;
                got_program_mode = 1;
                break;
            case 'b':
                blend_mode = optarg;
                got_blend_mode = 1;
                break;
            case 's':
                step = atoi(optarg);
                break;
            case 't':
                impression_count = atoi(optarg);
                break;
            case 'p':
                reference_image_filename = optarg;
                got_reference_image_filename = 1;
                break;
            case 'f':
                output_name = optarg;
                break;
            case 'v':
                verbose = 1;
                break;
            case 'h':
                fprintf(stderr, "USAGE: %s -i <INPUT DIR> -o <OUTPUT DIR> "
                "-l <FRAME COUNT> -m <PROGRAM MODE> -b <BLEND MODE>\n\n"

                "Program modes: 'single', 'tail-sequence'\n"
                "Blend modes: 'darker-color', 'lighter-color', 'further-from'\n\n"

                "Additional arguments:\n"
                "  -s\t"    "step - blend every nth frame\n"
                "  -t\t"    "impression count - the length of the 'tail' used in `tail-sequence' program mode\n"
                "  -p\t"    "reference background image - used with `further-from' blend mode\n"
                "  -f\t"    "output name - used in `single' program mode\n"
                "  -v\t"    "verbose - print lots of status messages\n"
                "  -h\t"    "print this message and exit\n"
                , argv[0]);
                return 0;
                break;
            case '?':
                if (optopt == 'c')
                    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                else if (isprint (optopt))
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n",
                optopt);
                return 1;
            default:
                abort ();
        }
    }

    for (index = optind; index < argc; index++) {
        printf("Non-option argument %s\n", argv[index]);
    }
    if (!got_input_dir) {
        printf("Required argument `i' not provided.\n");
    }
    if (!got_output_dir) {
        printf("Required argument `o' not provided.\n");
    }
    if (!got_frame_count) {
        printf("Required argument `l' not provided.\n");
    }
    if (!got_program_mode) {
        printf("Required argument `m' not provided.\n");
    }
    if (!got_blend_mode) {
        printf("Required argument `b' not provided.\n");
    }
    if (!got_input_dir ||
        !got_output_dir ||
        !got_frame_count ||
        !got_program_mode ||
        !got_blend_mode) {
        return 0;
    }

    // Ensure the input strings aren't too long
    if (strlen(input_dir) > MAX_FILENAME_LENGTH) {
        fprintf(stderr, "Input directory path is too long (%lu characters). "
        "Must be less than %d.\n", strlen(input_dir), MAX_FILENAME_LENGTH);
        return 1;
    }
    if (strlen(output_dir) > MAX_FILENAME_LENGTH) {
        fprintf(stderr, "Output directory path is too long (%lu characters). "
        "Must be less than %d.\n", strlen(output_dir), MAX_FILENAME_LENGTH);
        return 1;
    }
    if (strlen(program_mode) > MAX_STRING_ARG_LENGTH) {
        fprintf(stderr, "Program mode string is too long (%lu characters). "
        "Must be less than %d.\n", strlen(program_mode), MAX_STRING_ARG_LENGTH);
        return 1;
    }
    if (strlen(blend_mode) > MAX_STRING_ARG_LENGTH) {
        fprintf(stderr, "Blend mode string is too long (%lu characters). "
        "Must be less than %d.\n", strlen(blend_mode), MAX_STRING_ARG_LENGTH);
        return 1;
    }
    if (
        reference_image_filename &&
        strlen(reference_image_filename) > MAX_FILENAME_LENGTH
    ) {
        fprintf(stderr, "Reference image filename is too long (%lu characters). "
        "Must be less than %d.\n", strlen(reference_image_filename), MAX_FILENAME_LENGTH);
        return 1;
    }
    if (output_name && strlen(output_name) > MAX_STRING_ARG_LENGTH) {
        fprintf(stderr, "Output name is is too long (%lu characters). "
        "Must be less than %d.\n", strlen(output_name), MAX_STRING_ARG_LENGTH);
        return 1;
    }

    // Validate program mode
    if (strncmp(program_mode, "single", MAX_STRING_ARG_LENGTH) == 0) {
        if (strncmp(blend_mode, "further-from", MAX_STRING_ARG_LENGTH) == 0) {
            fprintf(stderr, "`further-from' blend mode not supported "
            "in `single' program mode.\n");
            return 1;
        }
    } else if (strncmp(program_mode, "tail-sequence", MAX_STRING_ARG_LENGTH) == 0) {
    } else {
        fprintf(stderr, "Invalid argument to option -m (program mode).\n");
        return 1;
    }

    // Handle blend mode
    const char* blend_shader;
    if (strncmp(blend_mode, "darker-color", MAX_STRING_ARG_LENGTH) == 0) {
        blend_shader = "blends/darker_color.frag";
    } else if (strncmp(blend_mode, "lighter-color", MAX_STRING_ARG_LENGTH) == 0){
        blend_shader = "blends/lighter_color.frag";
    } else if (strncmp(blend_mode, "further-from", MAX_STRING_ARG_LENGTH) == 0){
        if (!got_reference_image_filename) {
            printf("`further-from' blend mode requires argument `p', but it was not provided.\n");
            return 0;
        }
        blend_shader = "blends/further_from.frag";
    } else {
        fprintf(stderr, "Invalid argument to option -b (blend mode).\n");
        return 1;
    }

    printf("\n");
    printf("Input dir:        %s\n", input_dir);
    printf("Output dir:       %s\n", output_dir);
    printf("Frame count:      %d\n", frame_count);
    printf("Program mode:     %s\n", program_mode);
    printf("Blend mode:       %s\n", blend_mode);
    printf("Step:             %d\n", step);
    printf("Impression count: %d\n", impression_count);
    printf("Reference image:  %s\n", reference_image_filename);
    printf("Output name:      %s\n", output_name);
    printf("Verbose:          %d\n", verbose);
    printf("\n");

    // Initialize DevIL
    ilInit();

    // Initialize GLFW
    GLFWwindow* window;
    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }

    // Seems necessary if running "core" profile
    // Set all the required options for GLFW
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    printf("GLFW version:     %s\n", glfwGetVersionString());

    // NOTE: It seems on my system, with the retina display, the window should
    // be half as big
    window = glfwCreateWindow(WIDTH / 2, HEIGHT / 2,
        "Shader Deanimation", NULL, NULL);
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);

    // Load OpenGL function pointers via Glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }
    glfwSetErrorCallback(error_callback);
    glfwSetKeyCallback(window, key_callback);

    // Load OpenGL function pointers via Glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }

    printf("OpenGL version:   %s\n", glGetString(GL_VERSION));

    // Setup
    if (!make_resources(blend_shader)) {
        fprintf(stderr, "Failed to load resources\n");
        return 1;
    }
    glUseProgram(resources.programs.blend_two_textures);
    glUniform1i(resources.uniforms.textures[0], 0);
    glUniform1i(resources.uniforms.textures[1], 1);
    glUniform1i(resources.uniforms.textures[2], 2);
    prepare_quad();

    // Blend
    if (strncmp(program_mode, "single", MAX_STRING_ARG_LENGTH) == 0) {
        return blend_single(
            window,
            input_dir,
            output_dir,
            output_name,
            frame_count,
            blend_mode
        );
    } else if (strncmp(program_mode, "tail-sequence", MAX_STRING_ARG_LENGTH) == 0){
        return blend_tail_sequence(
            window,
            input_dir,
            output_dir,
            frame_count,
            blend_mode,
            step,
            impression_count,
            reference_image_filename
        );
    }

    // Teardown
    glfwSwapBuffers(window);
    glfwPollEvents();

    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
    return 0;
}
