#version 130
#if GL_EXT_texture_array
#extension GL_EXT_texture_array : enable
#endif

uniform sampler2DArray arraytexture;
varying vec2 texcoord;

void main() {
    vec4 a = texture2DArray(arraytexture, texcoord, 0);

    // vec4 a = texture2DArray()

    // vec4 a = texture(arraytexture, texcoord, 0);
    // vec4 a = vec4(0.3, 0.4, 0.5, 1.0);
    gl_FragColor = a;
}
