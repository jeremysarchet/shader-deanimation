#version 110

uniform sampler2D textures[2];

varying vec2 texcoord;

void main() {
    vec4 a = texture2D(textures[0], texcoord);
    vec4 b = texture2D(textures[1], texcoord);

    gl_FragColor = max(a, b);
}
