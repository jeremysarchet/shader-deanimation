#version 110

float overlay(float a, float b) {
	return a < 0.5 ? (2.0 * a * b) : (1.0 - 2.0 * (1.0 - a) * (1.0 - b));
}

vec3 overlay(vec3 a, vec3 b) {
	return vec3(overlay(a.r, b.r), overlay(a.g, b.g), overlay(a.b, b.b));
}

uniform sampler2D textures[2];

varying vec2 texcoord;

void main() {
    vec3 a = texture2D(textures[0], texcoord).rgb;
    vec3 b = texture2D(textures[1], texcoord).rgb;

    vec3 darken = min(a, b);
    vec3 lighten = max(a, b);

    vec3 blend = overlay(darken, lighten);

    gl_FragColor = vec4(blend, 1.0);
}
