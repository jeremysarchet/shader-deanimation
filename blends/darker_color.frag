#version 110

uniform sampler2D textures[2];
varying vec2 texcoord;

float luminance(vec4 color) {
    return 0.2126 * color.r + 0.7152 * color.g + 0.0722 * color.b;
}

void main() {
    vec4 col;
    vec4 a = texture2D(textures[0], texcoord);
    vec4 b = texture2D(textures[1], texcoord);

    float lumA = luminance(a);
    float lumB = luminance(b);

    if (lumA < lumB) {
        col = a;
    } else {
        col = b;
    }

    gl_FragColor = col;
}
