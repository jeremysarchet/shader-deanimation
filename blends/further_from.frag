#version 110

uniform sampler2D textures[3];
varying vec2 texcoord;

void main() {
    vec4 col;
    vec4 a = texture2D(textures[0], texcoord);
    vec4 b = texture2D(textures[1], texcoord);
    vec4 r = texture2D(textures[2], texcoord);

    // Weighted Euclidean idea from: https://www.compuphase.com/cmetric.htm
    float a_dist = sqrt(3.0*(r.r-a.r)*(r.r-a.r) + 4.0*(r.g-a.g)*(r.g-a.g) + 2.0*(r.b-a.b)*(r.b-a.b));
    float b_dist = sqrt(3.0*(r.r-b.r)*(r.r-b.r) + 4.0*(r.g-b.g)*(r.g-b.g) + 2.0*(r.b-b.b)*(r.b-b.b));

    vec4 further;
    float threshold = 0.15;

    // Find which color is further from the reference color
    if (a_dist > b_dist) {
        col = a;
    } else {
        col = b;
    }

    // Give b precedence if it is above the threshold
    // if (b_dist > threshold) {
    //     col = b;
    // }

    gl_FragColor = col;
}
