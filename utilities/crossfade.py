import sys


def abridge_seamless_frame_recipes(keeper_intervals, crossfade_duration):
    """Helper function for `abridge_seamless`.

    Builds up a recipe for each output frame in the sequence.
    For frames contained in an interval, take each of those frames as-is
    For the crossfades, blend the needed intermediate frames together with a
    smooth ramp of alpha.

    e.g.

        keeper_intervals = [[10, 13], [55, 58]]

        would result in the following value for `frame_recipes`:
            [('plain', 10),
            ('plain', 11),
            ('plain', 12),
            ('blend', [13, 51, 0.2]),
            ('blend', [14, 52, 0.4]),
            ('blend', [15, 53, 0.6]),
            ('blend', [16, 54, 0.8]),
            ('plain', 55),
            ('plain', 56),
            ('plain', 57),
            ('blend', [58, 6, 0.2]),
            ('blend', [59, 7, 0.4]),
            ('blend', [60, 8, 0.6])]
    """
    print("\n\nCreating seamless abridged sequence")

    frame_recipes = []

    # Ensure that all the gaps are sufficiently large to accommodate crossfades
    for interval in keeper_intervals:
        i = keeper_intervals.index(interval)

        # Accommodate the final crossfade that wraps around to the beginning
        if i == 0:
            gap = interval[0]
            if gap < crossfade_duration * 2:
                raise Exception("The first interval {} needs to start no earlier than {}".format(interval, crossfade_duration * 2))

        # Accommodate interstitial crossfades
        else:
            previous_interval = keeper_intervals[i - 1]
            gap = interval[0] - previous_interval[1]

            if gap < crossfade_duration * 2:
                raise Exception("The gap between intervals {} and {} needs to be at least {}".format(previous_interval, interval, crossfade_duration * 2))

    for interval in keeper_intervals:
        i = keeper_intervals.index(interval)

        # Add the plain frame recipes for this interval
        for j in range(interval[0], interval[1]):
            frame_recipes.append(("plain", j))

        # Get a handle to the next interval. At the end wrap back to the first
        if i < len(keeper_intervals) - 1:
            next_interval = keeper_intervals[i + 1]
        else:
            break  # Don't break if crossfade around the end is desired
            next_interval = keeper_intervals[0]

        # Add the blended frame recipes between this interval and next
        for j in range(crossfade_duration):
            alpha = (j + 1) / (crossfade_duration + 1)
            frame_recipes.append(("blend", [interval[1] + j, next_interval[0] - crossfade_duration + j, alpha]))

    return frame_recipes


def abridge_seamless(input_directory, output_directory, keeper_intervals, crossfade_duration):
    """Like `abridge` but adds crossfades in between the jumps.

    `keeper_intervals` needs to have length of at least 2
    """

    os.mkdir(output_directory)

    frame_recipes = abridge_seamless_frame_recipes(keeper_intervals, crossfade_duration)

    for frame in frame_recipes:
        output_index = frame_recipes.index(frame)

        type, recipe = frame[0], frame[1]

        if type == "plain":
            index = recipe
            filename = FRAME_PATTERN % index
            source_filepath = os.path.join(input_directory, filename)
            copy_filepath = os.path.join(output_directory, filename)

            # Copy and renumber
            shutil.copy(source_filepath, copy_filepath)
            renumbered_filename = FRAME_PATTERN % output_index
            renumbered_filepath = os.path.join(output_directory, renumbered_filename)
            os.rename(copy_filepath, renumbered_filepath)
            print("Took frame {} and copied it as frame {}".format(index, output_index))

        elif type == "blend":
            a_index, b_index, alpha = recipe
            A = Image.open(os.path.join(input_directory, FRAME_PATTERN % a_index))
            B = Image.open(os.path.join(input_directory, FRAME_PATTERN % b_index))

            # Blend and save
            blend_image = Image.blend(A, B, alpha)
            blend_image.save(os.path.join(output_directory, FRAME_PATTERN % output_index))
            print("Took frame {} and blended frame {} on top of it to make output frame {} with alpha: {}".format(a_index, b_index, output_index, round(alpha, 2)))


if __name__ == "__main__":
    input_directory = sys.argv[1]
    output_directory = sys.argv[2]
    keeper_intervals = sys.argv[3]

    # Modify keeper intervals to be a list of pairs of ints
    keeper_intervals = keeper_intervals.split(", ")
    keeper_intervals = [[int(i.split("-")[0]), int(i.split("-")[1])] for i in keeper_intervals]

    crossfade_duration = 48

    print("\nApplying crossfades between intervals:")
    print("Input directory: ", input_directory)
    print("Output directory: ", output_directory)
    print(keeper_intervals)
    abridge_seamless(input_directory, output_directory, keeper_intervals, crossfade_duration)
