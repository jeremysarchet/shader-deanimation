INGESTION_DIR='/Volumes/miro/Dropbox/deanimation/ingest'

# Enable null glob
shopt -s nullglob

videos=("${INGESTION_DIR}"/*.{MOV,mp4})
echo "Found ${#videos[@]} videos to ingest...\n"

original_ifs="$IFS"
IFS=$'\n'

for video in ${videos[@]}; do
  identifier=$(./identifier.sh $video);
  destination_dir="${INGESTION_DIR}/${identifier}"

  echo "===================Ingesting================"
  echo "Video:           ${video}"
  echo "Identifier:      ${identifier}"
  echo "Destination dir: ${destination_dir}"

  if [ -d $destination_dir ]; then
    echo "Directory '${destination_dir}' already exists. Skipping ingestion of this video.\n"
    continue
  fi

  mkdir $destination_dir

  echo 'Generating preview pair...'
  ./preview.sh $video $identifier $destination_dir
  echo

  echo 'Moving raw video into destination dir...'
  mv $video $destination_dir
done;

IFS=$original_ifs
