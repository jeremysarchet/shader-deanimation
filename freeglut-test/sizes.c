/*
 * Printing information about the program to the screen
 *
 * References:
 * https://www.opengl.org/archives/resources/code/samples/glut_examples/examples/examples.html
 * https://stackoverflow.com/questions/7879843/opengl-hello-world-on-mac-without-xcode
 *
 * I'm able to build it with `gcc -framework glut -framework opengl hey.c -o hey`
 */

#include <string.h>
#include <stdio.h>

// Seems lots of deprecation warnings from macOS can be silenced with this
#define GL_SILENCE_DEPRECATION
#include <GL/freeglut.h>

void *font = GLUT_BITMAP_HELVETICA_18;

void text(int x, int y, char *string) {
  // Iterate through the string drawing one character at a time
  int len, i;
  glRasterPos2f(x, y);
  len = (int) strlen(string);
  for (i = 0; i < len; i++) {
    glutBitmapCharacter(font, string[i]);
  }
}

void display(void) {
    glClearColor(0.9, 0.9, 0.9, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3d(0.7, 0.8, 0.9);

    // Draw a rectangle inset from the window edges
    int w = glutGet(GLUT_WINDOW_WIDTH);
    int h = glutGet(GLUT_WINDOW_HEIGHT);
    int margin = 50;
    glRecti(margin, margin, w - margin, h - margin);

    // Set the color to dark grey in preparation for drawing some text
    glColor3d(0.1, 0.1, 0.1);

    // Draw window width and height and time
    int ms = glutGet(GLUT_ELAPSED_TIME);
    char width_string[80];
    char height_string[80];
    char milliseconds_string[80];
    sprintf(width_string, "Width: %d", w);
    sprintf(height_string, "Height: %d", h);
    sprintf(milliseconds_string, "Milliseconds: %d", ms);
    text(150, 150, width_string);
    text(150, 160, height_string);
    text(150, 170, milliseconds_string);

    glutSwapBuffers();
}

void reshape(int width, int height) {
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //set the coordinate system, with the origin in the top left
    gluOrtho2D(0, width, height, 0);
    glMatrixMode(GL_MODELVIEW);
}

void idle(void) {
    glutPostRedisplay();
}

int main(int argc, char **argv) {
    //a basic set up...
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB);
    glutInitWindowSize(100, 100);

    //create the window, the argument is the title
    glutCreateWindow("GLUT program");

    //pass the callbacks
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);

    //glutFullScreen();
    // Reshaping the window before starting the main loop seems to get it
    // displaying correctly from the get-go. If not, the coordinates seem to
    // take up the bottom-left 1/4th of the window area, which I suspect is a
    // symptom of the high-PPI display on my macbook. This 1/4th thing would
    // otherwise be resolved by resizing the window, which seems to make the
    // application suddenly high-PPI aware.
    glutReshapeWindow(640, 480);
    glutMainLoop();

    //we never get here because glutMainLoop() is an infinite loop
    return 0;
}
