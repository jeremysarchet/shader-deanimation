# Reproduction of inexplicable xfade behavior. The output should be ~9 seconds
# long but is only ~6 seconds long becaust it cuts out right at the end of the
# last crossfade.

ffmpeg \
  -i '/Users/jbs/projects/shader-deanimation/concat-test/0.mp4' \
  -i '/Users/jbs/projects/shader-deanimation/concat-test/1.mp4' \
  -filter_complex \
    "
    [0][1]xfade=wipedown:duration=0.5:offset=2.5[s2]; \
    [s2][0]xfade=wipedown:duration=0.5:offset=5.0[s3] \
    " \
  -map "[s3]" \
  -pix_fmt yuv420p \
  basic-xfade-test.mp4 \
