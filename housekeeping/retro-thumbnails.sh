# WARNING: Temporary script for retroactively generating preview thumbnails for
# videos already ingested. Not intended for production use.

year=$1
dir="/Volumes/miro/Dropbox/deanimation/raw/$year"

for f in $dir/*/*.jpg; do
  cp "$f" `echo "$f" | sed s/\preview.jpg$/preview-thumbnail.jpg/`;
done;

sips -Z 200 $dir/*/*preview-thumbnail.jpg
