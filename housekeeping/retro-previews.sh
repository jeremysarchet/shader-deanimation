# WARNING: Temporary script for retroactively generating previews for videos
# already ingested. Not intended for production use.

year=$1
ingestion_dir="/Volumes/miro/Dropbox/deanimation/raw/$year"

# Enable null glob
shopt -s nullglob

#videos=("${ingestion_dir}"/*/*.mov)
videos=("${ingestion_dir}"/*/*.MOV)

echo "Found ${#videos[@]} videos to ingest...\n"

for video in ${videos[@]}; do
  identifier=$(./identifier.sh $video);
  destination_dir="${ingestion_dir}/${identifier}"

  echo "===================Ingesting================"
  echo "Video:           ${video}"
  echo "Identifier:      ${identifier}"
  echo "Destination dir: ${destination_dir}"

  echo 'Generating previews...'
  ./preview.sh $video $identifier $destination_dir

  echo
done;
