# Convert a dict of curve points to ffmpeg compatible curves string

def generate_coordinates(points):
    result = ""
    points = [[round((c / 255.0), 2) for c in point] for point in points]
    for i, point in enumerate(points):
        result += "{}/{}".format(point[0], point[1])
        if (i + 1) < len(points):
            result += " "
    return result


def generate_curves_string(data):
    if len(data) == 1:
        all = generate_coordinates(data["all"])
        return "master: '{}'".format(all)
    elif len(data) == 3:
        r = generate_coordinates(data["r"])
        g = generate_coordinates(data["g"])
        b = generate_coordinates(data["b"])
        return "red: '{}'\ngreen: '{}'\nblue: '{}'".format(r, g, b)
    else:
        return ""


if __name__ == "__main__":
    curves = {
        "r":[[7, 0], [69, 68], [162, 199], [207, 255]],
        "g":[[4, 0], [72, 64], [161, 203], [202, 255]],
        "b":[[8, 0], [76, 61], [171, 209], [222, 255]]
    }

    curves = {"all":[[0, 0], [135, 255]]}

    print(generate_curves_string(curves))
