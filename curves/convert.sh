# Given an input "old style curves" file from GIMP, reformat it for ffmpeg

# Ensure exactly two arguments supplied
if [[ $# -ne 1 ]] ; then
    echo 'Usage: ./convert.sh <curves-filepath>'
    exit 1
fi

function convert {
    # Given an integer index `$1`, tokenize the line from `$lines[index]`,
    # convert the tokens to the range 0-1 and echo them as forward slash
    # delimited ordered pairs
    IFS=" " read -a tokens <<< "${lines[$1]}"
    for i in ${!tokens[@]}
    do
        coordinate="${tokens[$i]}"
        if [ $coordinate != "-1" ]
        then
            answer=$(bc <<< "scale=2; ${coordinate}/255")
            printf "%1.2f" ${answer}
            if [ $(($i % 2)) -eq 0 ]
            then
                printf "/"
            else
                printf " "
            fi
        fi
    done
}

# Read each line of the file into an array
IFS=$'\n' read -d '' -a lines < $1

# Convert the corresponding lines and echo a string ready for ffmpeg
echo "master: '$(convert 1)'"
echo "red:    '$(convert 2)'"
echo "green:  '$(convert 3)'"
echo "blue:   '$(convert 4)'"
