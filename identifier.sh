# Given a video, returns an identifier for it based on creation time
#
# Date and time
# https://superuser.com/questions/841235/how-do-i-use-ffmpeg-to-get-the-video-resolution
# https://stackoverflow.com/questions/31541489/ffmpeg-get-creation-time-and-or-modification-date

# Ensure exactly one argument supplied
if [[ $# -ne 1 ]] ; then
    echo 'Usage: ./identifier.sh <video-path>'
    exit 1
fi

video_path="$1"

result=$(ffprobe \
    -loglevel error \
    "$video_path" \
    -print_format compact=print_section=0:nokey=1:escape=csv \
    -show_entries format_tags=creation_time
)

# Ensure creation time can be read
if [[ ! $result ]] ; then
    echo "Unable to determine video creation time"
    exit 1
fi

result="${result:0:19}" # Gets us e.g. 2024-01-01T12:34:56
result="${result/T/-}"  # Gets us e.g. 2024-01-01-12:34:56
result="${result//:/}"  # Gets us e.g. 2024-01-01-123456

echo $result
