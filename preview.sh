#!/bin/bash

# Produce deanimation "still previews" from raw source videos.
#
# A good "still preview" gives an idea of what subjects passed through the
# frame during the clip. It's sometimes tedious to sit and watch through a raw
# video and try to see if any subjects passed through, or if it was a whole lot
# of nothing. So, this can best be served by working with only every nth frame.
# The resulting image is less crowded, and the processing time is faster.
#
# So the extraction and blending goes like this:
# 1. Extract every nth frame into an image sequence via FFMPEG
# 2. Blend each image from the extracted sequence via `tail_sequence`. Note, we
#    pass a `step` of 1, so we blend every image from the extracted sequence.
#    Don't take this to mean that we are ending up with a generated output
#    covering every source frame though.

STEP=7

video_path=$1
video_id=$2
output_dir=$3
echo 'Generating preview at: '$output_dir

# Read in global configuration parameters
scratch_dir=$(yq e ".scratch_dir" global.yml)/still-previews
frame_basename_pattern=$(yq e ".frame_basename_pattern" global.yml)
frame_file_extension=$(yq e ".frame_file_extension" global.yml)
extraction_dir="${scratch_dir}/${video_id}-extracted"

extraction_frame_pattern=$extraction_dir/$frame_basename_pattern.$frame_file_extension

function extract() {
    printf "\n---------Extracting for preview----------\n"
    # Overwrite
    if [ -d "$extraction_dir" ]; then
      rm -r $extraction_dir
      echo
      echo "Directory $extraction_dir exists. Removing.\n"
    fi
    mkdir -p $extraction_dir
    ffmpeg \
        -i "$video_path" \
        -vf "select=not(mod(n\,${STEP}))" \
        -start_number 0 \
        -vf "scale=1920x1080" \
        $extraction_frame_pattern \
        -vsync "vfr" \
        -loglevel panic \
        -hide_banner
}

function single() {
    # Count the number of frames to be deanimated
    deanimation_frames=$(find ${extraction_dir} -name *.$frame_file_extension | wc -l | xargs)
    # Deanimate
    printf "\n--------------Preview single--------------\n"
    echo "Frame pattern:      ${extraction_frame_pattern}"
    echo "Deanimation frames: ${deanimation_frames}"
    echo "Mode:               ${1}"

    # NOTE: Including the step in the filename (e.g. blend every 3rd frame)
    # with the "s" prefix (even though the thing that gives us this is
    # technically the `mod` from the extraction step).
    ./tail_sequence \
        -i $extraction_dir \
        -o $output_dir \
        -l $deanimation_frames \
        -m 'single' \
        -b $1 \
        -s 1 \
        -f "${video_id}-s${STEP}-${1}-preview"
}

extract

single 'lighter-color'
single 'darker-color'

# Make thumbnails of the preview images
for f in $output_dir/*.jpg; do
  cp "$f" `echo "$f" | sed s/\preview.jpg$/preview-thumbnail.jpg/`;
done;
sips -Z 200 $output_dir/*preview-thumbnail.jpg

rm -r $extraction_dir
