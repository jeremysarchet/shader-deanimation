import os
import subprocess
from PIL import Image, ImageChops

images = []

for i in range(100):
    filepath = "frames/{}.png".format(i)
    images.append(Image.open(filepath))

result = images[0]

for image in images:
    result = ImageChops.darker(result, image);

result.save("output/multi-blend-py.png")
