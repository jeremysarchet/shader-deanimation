"""Figuring out how to iterate blends over multiple shader passes."""

N = 10
bound_fbo = None
working = None
A = "A"
B = "B"

def bind_framebuffer(fbo):
    global bound_fbo
    bound_fbo = fbo

def bind_texture(fbo):
    global working
    working = fbo

print("Initialize A with frames[0]")

for i in range(1, N):
    if i % 2 == 0:
        bind_framebuffer(A)
        bind_texture(B)
    else:
        bind_framebuffer(B)
        bind_texture(A)

    print("Render blend({}, frames[{}]) into {}".format(working, i, bound_fbo))

print("Save pixels from {}".format(bound_fbo))
