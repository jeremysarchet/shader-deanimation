/*
 * Derived from:
 *
 * "Hello GL"
 * http://duriansoftware.com/joe/An-intro-to-modern-OpenGL.-Table-of-Contents.html
 * https://github.com/jckarter/hello-gl
 *
 * I've been able to compile it with
 * `gcc array_blend.c include/glad.c -lGLFW -lIL -framework Cocoa -framework OpenGL -framework IOKit`
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <IL/il.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

unsigned int load_image(const char *filename) {
    // Load an image at `filename` and return its DevIL id.
    ILuint image_id;
    // printf("Loading image: %s\n", filename);
    ilGenImages(1, &image_id);
    ilBindImage(image_id);
    ilEnable(IL_ORIGIN_SET);
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
    ilLoadImage(filename);
    // QUESTION: Does this do unnecessary work if the image is already RGB?
    ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
    // printf("Width: %d, Height %d, Bytes per Pixel %d\n", ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL));
    return image_id;
}

void write_image(char *filepath, int width, int height, void *buffer) {
    // Save an image to disk at `filepath` given image data in `buffer`.
    ILuint image;
    int pixel_size = 1 * 3; // 3 bytes for RGB
    ilGenImages(1, &image);
    ilBindImage(image);
    ilTexImage(width, height, 0, pixel_size, IL_RGB, IL_UNSIGNED_BYTE, buffer);
    ilSaveImage(filepath);
    ilDeleteImage(image);
}

void capture_image(GLFWwindow* window, char *filename) {
    // Save the framebuffer
    int fb_width, fb_height;
    glfwGetFramebufferSize(window, &fb_width, &fb_height);
    printf("Framebuffer width: %d, height: %d\n", fb_width, fb_height);
    // Create a buffer to store the image data
    void *output;
    output = malloc(fb_width * fb_height * 3);
    printf("Saving image with width: %d, height: %d\n", fb_width, fb_height);
    glReadPixels(0, 0, fb_width, fb_height, GL_RGB, GL_UNSIGNED_BYTE, output);
    // Write the image data with DevIL
    write_image(filename, fb_width, fb_height, output);
}

void *file_contents(const char *filename, GLint *length) {
    FILE *f = fopen(filename, "r");
    void *buffer;

    if (!f) {
        fprintf(stderr, "Unable to open %s for reading\n", filename);
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);

    buffer = malloc(*length+1);
    *length = fread(buffer, 1, *length, f);
    fclose(f);
    ((char*)buffer)[*length] = '\0';

    return buffer;
}

// Global data for our program
static struct {
    GLuint vertex_buffer, element_buffer;
    GLuint arraytexture;
    GLuint vertex_shader, fragment_shader, program;

    struct {
        GLint arraytexture;
    } uniforms;

    struct {
        GLint position;
    } attributes;
} resources;

static GLuint make_buffer(GLenum target, const void *buffer_data,
    GLsizei buffer_size) {
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(target, buffer);
    glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);
    return buffer;
}

static GLuint make_arraytexture() {
    int width, height;
    width = 1920;
    height = 1080;
    unsigned char* data;
    ILuint image_id;
    GLuint arraytexture;

    int frame_count = 3;
    void *images[frame_count];
    int i;
    for (i = 0; i < frame_count; ++i) {
        images[i] = malloc(width * height * 3);
    }

    // Load image with DevIL and copy its data into the array of buffers
    image_id = load_image("frames/50.png");
    ilBindImage(image_id);
    ilCopyPixels(0, 0, 0, width, height, 1, IL_RGB, IL_UNSIGNED_BYTE, images[0]);
    ilDeleteImage(image_id);

    image_id = load_image("frames/55.png");
    ilBindImage(image_id);
    ilCopyPixels(0, 0, 0, width, height, 1, IL_RGB, IL_UNSIGNED_BYTE, images[1]);
    ilDeleteImage(image_id);

    image_id = load_image("frames/60.png");
    ilBindImage(image_id);
    ilCopyPixels(0, 0, 0, width, height, 1, IL_RGB, IL_UNSIGNED_BYTE, images[2]);
    ilDeleteImage(image_id);

    // Make an array texture with the image data
    glGenTextures(1, &arraytexture);
    glBindTexture(GL_TEXTURE_2D_ARRAY_EXT, arraytexture);
    // Set reasonable texture parameters
    glTexParameteri(GL_TEXTURE_2D_ARRAY_EXT, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY_EXT, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY_EXT, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D_ARRAY_EXT, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
    // Allocate the storage
    // glTexStorage3D(GL_TEXTURE_2D_ARRAY_EXT, 1, GL_RGB8, width, height, frame_count);

    data = (unsigned char *) images[0];

    // Upload pixel data
    glTexImage3D(
        GL_TEXTURE_2D_ARRAY_EXT, 0,     // target, level
        GL_RGB8,                    // internal format
        width, height, frame_count, // width, height, depth
        0,                          // border (legacy, must be 0)
        GL_RGB, GL_UNSIGNED_BYTE,   // external format, type
        data                        // pixels
    );
    // Return the arraytexture
    return arraytexture;
}

static void show_info_log(GLuint object, PFNGLGETSHADERIVPROC glGet__iv,
    PFNGLGETSHADERINFOLOGPROC glGet__InfoLog) {
    GLint log_length;
    char *log;

    glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);
    log = malloc(log_length);
    glGet__InfoLog(object, log_length, NULL, log);
    fprintf(stderr, "%s", log);
    free(log);
}

static GLuint make_shader(GLenum type, const char *filename) {
    GLint length;
    GLchar *source = file_contents(filename, &length);
    GLuint shader;
    GLint shader_ok;

    if (!source)
        return 0;

    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)&source, &length);
    free(source);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        fprintf(stderr, "Failed to compile %s:\n", filename);
        show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}

static GLuint make_program(GLuint vertex_shader, GLuint fragment_shader) {
    GLint program_ok;
    GLuint program = glCreateProgram();

    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        fprintf(stderr, "Failed to link shader program:\n");
        show_info_log(program, glGetProgramiv, glGetProgramInfoLog);
        glDeleteProgram(program);
        return 0;
    }
    return program;
}

// Data used to seed our vertex array and element array buffers:
static const GLfloat g_vertex_buffer_data[] = {
    -1.0f, -1.0f,
     1.0f, -1.0f,
    -1.0f,  1.0f,
     1.0f,  1.0f
};
static const GLushort g_element_buffer_data[] = { 0, 1, 2, 3 };


// Load and create all of our resources:
static int make_resources(void) {
    resources.vertex_buffer = make_buffer(
        GL_ARRAY_BUFFER,
        g_vertex_buffer_data,
        sizeof(g_vertex_buffer_data)
    );
    resources.element_buffer = make_buffer(
        GL_ELEMENT_ARRAY_BUFFER,
        g_element_buffer_data,
        sizeof(g_element_buffer_data)
    );

    resources.arraytexture = make_arraytexture();

    if (resources.arraytexture == 0)
        return 0;

    resources.vertex_shader = make_shader(GL_VERTEX_SHADER, "blend.vert");
    if (resources.vertex_shader == 0)
        return 0;

    resources.fragment_shader = make_shader(GL_FRAGMENT_SHADER, "blends/array_min.frag");
    if (resources.fragment_shader == 0)
        return 0;

    resources.program = make_program(
        resources.vertex_shader,
        resources.fragment_shader
    );
    if (resources.program == 0)
        return 0;

    resources.uniforms.arraytexture
        = glGetUniformLocation(resources.program, "arraytexture");
    resources.attributes.position
        = glGetAttribLocation(resources.program, "position");

    return 1;
 }

static void render(void) {
    glClear(GL_COLOR_BUFFER_BIT);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D_ARRAY_EXT, resources.arraytexture);
    glUniform1i(resources.uniforms.arraytexture, 0);

    glBindBuffer(GL_ARRAY_BUFFER, resources.vertex_buffer);
    glVertexAttribPointer(
        resources.attributes.position,  /* attribute */
        2,                                /* size */
        GL_FLOAT,                         /* type */
        GL_FALSE,                         /* normalized? */
        sizeof(GLfloat)*2,                /* stride */
        (void*)0                          /* array buffer offset */
    );
    glEnableVertexAttribArray(resources.attributes.position);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resources.element_buffer);
    glDrawElements(
        GL_TRIANGLE_STRIP,  /* mode */
        4,                  /* count */
        GL_UNSIGNED_SHORT,  /* type */
        (void*)0            /* element array buffer offset */
    );

    glDisableVertexAttribArray(resources.attributes.position);
}

static void key_callback(GLFWwindow* window, int key, int scancode,
    int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

static void error_callback(int error, const char* description) {
    fputs(description, stderr);
}

int main(int argc, char** argv) {
    GLFWwindow* window;

    // Initialize DevIL
    ilInit();

    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }
    window = glfwCreateWindow(960, 540, "Simple example", NULL, NULL);
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    // Load OpenGL function pointers via Glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }

    printf("OpenGL version: %s\n", glGetString(GL_VERSION));
    printf("GLSL version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    printf("Max texture image units: %s\n", glGetString(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS));

    glfwSetErrorCallback(error_callback);
    glfwSetKeyCallback(window, key_callback);

    // Load OpenGL function pointers via Glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }

    if (!make_resources()) {
        fprintf(stderr, "Failed to load resources\n");
        return 1;
    }

    glUseProgram(resources.program);

    while (!glfwWindowShouldClose(window)) {
        render();

        // capture_image(window, "output/result.png");
        // break;

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
    return 0;
}
