/*
 * Derived from:
 *
 * "Hello GL"
 * http://duriansoftware.com/joe/An-intro-to-modern-OpenGL.-Table-of-Contents.html
 * https://github.com/jckarter/hello-gl
 *
 * I've been able to compile it with
 * `gcc multi_blend.c include/glad.c -lGLFW -lIL -framework Cocoa -framework OpenGL -framework IOKit`
 *
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <IL/il.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

unsigned int load_image(const char *filename) {
    // Load an image at `filename` and return its DevIL id.
    ILuint image_id;
    // printf("Loading image: %s\n", filename);
    ilGenImages(1, &image_id);
    ilBindImage(image_id);
    ilEnable(IL_ORIGIN_SET);
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
    ilLoadImage(filename);
    // QUESTION: Does this do unnecessary work if the image is already RGB?
    ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
    // printf("Width: %d, Height %d, Bytes per Pixel %d\n", ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL));
    return image_id;
}

void write_image(char *filepath, int width, int height, void *buffer) {
    // Save an image to disk at `filepath` given image data in `buffer`.
    ILuint image;
    int pixel_size = 1 * 3; // 3 bytes for RGB
    ilGenImages(1, &image);
    ilBindImage(image);
    ilTexImage(width, height, 0, pixel_size, IL_RGB, IL_UNSIGNED_BYTE, buffer);
    ilSaveImage(filepath);
    ilDeleteImage(image);
}

void capture_image(GLFWwindow* window, char *filename) {
    // Save the framebuffer
    int fb_width, fb_height;
    glfwGetFramebufferSize(window, &fb_width, &fb_height);
    printf("Framebuffer width: %d, height: %d\n", fb_width, fb_height);
    // Create a buffer to store the image data
    void *output;
    output = malloc(fb_width * fb_height * 3);
    printf("Saving image with width: %d, height: %d\n", fb_width, fb_height);
    glReadPixels(0, 0, fb_width, fb_height, GL_RGB, GL_UNSIGNED_BYTE, output);
    // Write the image data with DevIL
    write_image(filename, fb_width, fb_height, output);
}

static void key_callback(GLFWwindow* window, int key, int scancode,
    int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
}

static void error_callback(int error, const char* description) {
    fputs(description, stderr);
}

void *file_contents(const char *filename, GLint *length) {
    FILE *f = fopen(filename, "r");
    void *buffer;

    if (!f) {
        fprintf(stderr, "Unable to open %s for reading\n", filename);
        return NULL;
    }

    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);

    buffer = malloc(*length+1);
    *length = fread(buffer, 1, *length, f);
    fclose(f);
    ((char*)buffer)[*length] = '\0';

    return buffer;
}

// Global data for our program
static struct {
    GLuint vertex_buffer, element_buffer;
    GLuint textures[2];
    GLuint vertex_shader;

    GLuint framebuffer_a, framebuffer_b;
    GLuint colorbuffer_a, colorbuffer_b;

    struct {
        GLuint blend_two_textures, show_texture;
    } fragment_shaders;

    struct {
        GLuint blend_two_textures, show_texture;
    } programs;

    struct {
        GLint textures[2];
    } uniforms;

    struct {
        GLint position;
    } attributes;
} resources;

static GLuint make_buffer(GLenum target, const void *buffer_data,
    GLsizei buffer_size) {
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(target, buffer);
    glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);
    return buffer;
}

void set_texture_parameters() {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
}

static GLuint make_texture(const char *filename) {
    int width, height;
    unsigned char* data;
    ILuint image_id;
    GLuint texture;

    // Load image with DevIL and get a handle to its data
    image_id = load_image(filename);
    ilBindImage(image_id);
    width = ilGetInteger(IL_IMAGE_WIDTH);
    height = ilGetInteger(IL_IMAGE_HEIGHT);
    data = ilGetData();

    // Make a texture with the image data
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    set_texture_parameters();
    glTexImage2D(
        GL_TEXTURE_2D, 0,           /* target, level */
        GL_RGB8,                    /* internal format */
        width, height, 0,           /* width, height, border */
        GL_RGB, GL_UNSIGNED_BYTE,   /* external format, type */
        data                        /* pixels */
    );
    // Delete the DevIL image and return the texture
    ilDeleteImage(image_id);
    return texture;
}

static GLuint make_colorbuffer() {
    GLuint colorbuffer;
    glGenTextures(1, &colorbuffer);
    glBindTexture(GL_TEXTURE_2D, colorbuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1920, 1080, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    set_texture_parameters();
    return colorbuffer;
}

static GLuint make_framebuffer(GLuint colorbuffer) {
    GLuint framebuffer;
    glGenFramebuffers(1, &framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorbuffer, 0);

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (status != GL_FRAMEBUFFER_COMPLETE) {
        fprintf(stderr, "Trouble with framebuffer\n");
        return 1;
    }

    return framebuffer;
}

static void show_info_log(GLuint object, PFNGLGETSHADERIVPROC glGet__iv,
    PFNGLGETSHADERINFOLOGPROC glGet__InfoLog) {
    GLint log_length;
    char *log;

    glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);
    log = malloc(log_length);
    glGet__InfoLog(object, log_length, NULL, log);
    fprintf(stderr, "%s", log);
    free(log);
}

static GLuint make_shader(GLenum type, const char *filename) {
    GLint length;
    GLchar *source = file_contents(filename, &length);
    GLuint shader;
    GLint shader_ok;

    if (!source)
        return 0;

    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)&source, &length);
    free(source);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        fprintf(stderr, "Failed to compile %s:\n", filename);
        show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}

static GLuint make_program(GLuint vertex_shader, GLuint fragment_shader) {
    GLint program_ok;
    GLuint program = glCreateProgram();

    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        fprintf(stderr, "Failed to link shader program:\n");
        show_info_log(program, glGetProgramiv, glGetProgramInfoLog);
        glDeleteProgram(program);
        return 0;
    }
    return program;
}

// Data used to seed our vertex array and element array buffers:
static const GLfloat g_vertex_buffer_data[] = {
    -1.0f, -1.0f,
     1.0f, -1.0f,
    -1.0f,  1.0f,
     1.0f,  1.0f
};
static const GLushort g_element_buffer_data[] = { 0, 1, 2, 3 };


// Load and create all of our resources:
static int make_resources(void) {
    resources.vertex_buffer = make_buffer(
        GL_ARRAY_BUFFER,
        g_vertex_buffer_data,
        sizeof(g_vertex_buffer_data)
    );
    resources.element_buffer = make_buffer(
        GL_ELEMENT_ARRAY_BUFFER,
        g_element_buffer_data,
        sizeof(g_element_buffer_data)
    );

    // resources.textures[0] = make_texture("frames/45.png");
    // resources.textures[1] = make_texture("frames/55.png");
    //
    // if (resources.textures[0] == 0 || resources.textures[1] == 0)
    //     return 0;

    resources.vertex_shader = make_shader(GL_VERTEX_SHADER, "blend.vert");
    if (resources.vertex_shader == 0)
        return 0;

    resources.fragment_shaders.blend_two_textures = make_shader(GL_FRAGMENT_SHADER, "blends/darker.frag");
    if (resources.fragment_shaders.blend_two_textures == 0)
        return 0;

    resources.fragment_shaders.show_texture = make_shader(GL_FRAGMENT_SHADER, "blends/show_texture.frag");
    if (resources.fragment_shaders.show_texture == 0)
        return 0;

    resources.programs.blend_two_textures = make_program(
        resources.vertex_shader,
        resources.fragment_shaders.blend_two_textures
    );
    if (resources.programs.blend_two_textures == 0)
        return 0;

    resources.programs.show_texture = make_program(
        resources.vertex_shader,
        resources.fragment_shaders.show_texture
    );
    if (resources.programs.show_texture == 0)
        return 0;

    resources.uniforms.textures[0]
        = glGetUniformLocation(resources.programs.blend_two_textures, "textures[0]");
    resources.uniforms.textures[1]
        = glGetUniformLocation(resources.programs.blend_two_textures, "textures[1]");
    resources.attributes.position
        = glGetAttribLocation(resources.programs.blend_two_textures, "position");

    resources.colorbuffer_a = make_colorbuffer();
    resources.framebuffer_a = make_framebuffer(resources.colorbuffer_a);
    resources.colorbuffer_b = make_colorbuffer();
    resources.framebuffer_b = make_framebuffer(resources.colorbuffer_b);

    return 1;
}

void prepare_quad() {
    glBindBuffer(GL_ARRAY_BUFFER, resources.vertex_buffer);
    glVertexAttribPointer(
        resources.attributes.position,  /* attribute */
        2,                                /* size */
        GL_FLOAT,                         /* type */
        GL_FALSE,                         /* normalized? */
        sizeof(GLfloat)*2,                /* stride */
        (void*)0                          /* array buffer offset */
    );
    glEnableVertexAttribArray(resources.attributes.position);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, resources.element_buffer);
}

void draw() {
    glDrawElements(
        GL_TRIANGLE_STRIP,  /* mode */
        4,                  /* count */
        GL_UNSIGNED_SHORT,  /* type */
        (void*)0            /* element array buffer offset */
    );
}

int main(int argc, char** argv) {
    GLFWwindow* window;

    // Initialize DevIL
    ilInit();

    if (!glfwInit()) {
        exit(EXIT_FAILURE);
    }
    window = glfwCreateWindow(960, 540, "Simple example", NULL, NULL);
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);
    // Load OpenGL function pointers via Glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }

    glfwSetErrorCallback(error_callback);
    glfwSetKeyCallback(window, key_callback);

    // Load OpenGL function pointers via Glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }

    if (!make_resources()) {
        fprintf(stderr, "Failed to load resources\n");
        return 1;
    }

    while (!glfwWindowShouldClose(window)) {
        int i;
        int frame_count = 100;
        char frame_path[20];
        GLint frames[frame_count];

        clock_t t;
        t = clock();

        // Load frames into textures
        for (i = 0; i < frame_count; ++i) {
            sprintf(frame_path, "frames/%d.png", i);
            // printf("Making texture: %s\n", frame_path);
            frames[i] = make_texture(frame_path);
        }

        t = clock() - t;
        double time_taken = ((double)t)/CLOCKS_PER_SEC;
        printf("Took %f seconds to load textures\n", time_taken);

        glUseProgram(resources.programs.blend_two_textures);
        glClear(GL_COLOR_BUFFER_BIT);

        glUniform1i(resources.uniforms.textures[0], 0);
        glUniform1i(resources.uniforms.textures[1], 1);

        prepare_quad();

        // First, blend 0 and 1 into A
        glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_a);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, frames[0]);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, frames[1]);
        draw();

        // Blend frames 2 through N from and to alternating framebuffers
        for (i = 2; i < frame_count; ++i) {
            if (i % 2 == 0) {
                // printf("Blending A and %d into B\n", i);
                glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_b);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_a);
            } else {
                // printf("Blending B and %d into A\n", i);
                glBindFramebuffer(GL_FRAMEBUFFER, resources.framebuffer_a);
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_b);
            }
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, frames[i]);
            draw();
        }

        t = clock() - t;
        time_taken = ((double)t)/CLOCKS_PER_SEC;
        printf("Took %f seconds to blend textures\n", time_taken);

        // Draw the result into the default framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glUseProgram(resources.programs.show_texture);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, resources.colorbuffer_a);
        draw();

        // Save the result
        char *output_filename = "output/result.png";
        remove(output_filename);
        capture_image(window, output_filename);
        break;

        // Cleanup
        glDisableVertexAttribArray(resources.attributes.position);
        glDeleteFramebuffers(1, &resources.framebuffer_a);
        glDeleteFramebuffers(1, &resources.framebuffer_b);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
    return 0;
}
