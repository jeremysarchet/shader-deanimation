/*
 * Tail sequence iteraction scheme.
 *
 */

#include <stdio.h>

#define FRAME_COUNT 30
#define STEP 3
#define IMPRESSION_COUNT 3
#define SPAN (IMPRESSION_COUNT * STEP)


int main() {
    int i, j;

    // Setup the tail
    static struct {
        int textures[IMPRESSION_COUNT];
        int cursor;
    } tail;
    tail.cursor = 0;

    // Populate the tail
    for (i = 0; i < IMPRESSION_COUNT; ++i) {
        tail.textures[i] = i * STEP;
    }

    // Print a label
    printf("--------------------------------\n");
    printf("follower | impressions | leader\n");
    printf("--------------------------------\n");

    // Iterate through the sequence
    for (i = SPAN; i < FRAME_COUNT; ++i) {
        // Print the follower
        printf("%2d", i - SPAN);
        printf(" | ");

        // When we're at a STEP, advance the impressions
        if (i % STEP == 0) {
            // glDeleteTextures(1, tail.textures[tail.cursor]);
            tail.textures[tail.cursor] = i; // make new texture and assign it
            tail.cursor += 1;
            tail.cursor %= IMPRESSION_COUNT;
        }

        // Print the impressions, oldest to newest
        for (j = 0; j < IMPRESSION_COUNT; ++j) {
            // printf("tail.textures[%d] = %d\n", i, tail.textures[i]);
            printf("%2d ", tail.textures[j]);
        }
        printf("| ");

        // Print the leader
        printf("%2d", i);

        // Print a mark if we're at a STEP
        if (i % STEP == 0) {
            printf(" *");
        }
        printf("\n");
    }
    return 0;
}
