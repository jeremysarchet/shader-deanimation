#!/bin/bash
# Shell script for creating deanimation video montages

CROSSFADE_DURATION = 2.0

# Ensure exactly one argument supplied
if [[ $# -ne 1 ]] ; then
    echo 'Usage: ./montages.sh <collection>'
    exit 1
fi

# Receive command line arguments as inputs
collection=$1

# Ensure there's an entry for the given year and id
if [[ ! $(yq r montages.yml "${collection}") ]] ; then
    echo "No montage recipe found for the given collection."
    exit 1
fi

# Read in global configuration parameters
processed_dir=$(yq r global.yml "processed_dir")

echo "Making montage for ${collection} collection"

videos=($(yq r montages.yml "${collection}.videos"))

paths=()
for i in "${videos[@]}"
do
   echo "$i"
   paths+=("${processed_dir}/${i}")
done

basename="${collection}-montage"
python3 crossfade.py "${basename}.mp4" $CROSSFADE_DURATION "${paths[@]}"

# Make a copy converted to a raw h264 stream
ffmpeg \
  -i "${basename}.mp4" \
  -vcodec copy \
  -an \
  -bsf:v h264_mp4toannexb \
  "${basename}.h264"
