/*
 * Derived from the following two example programs:
 *
 * "Textures Combined"
 * https://learnopengl.com/Getting-started/Textures
 * https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/4.2.textures_combined/textures_combined.cpp
 *
 * "Hello Triangle"
 * https://antongerdelan.net/opengl/hellotriangle.html
 * https://github.com/capnramses/antons_opengl_tutorials_book/blob/master/00_hello_triangle/main.c
 *
 * I've been able to compile it with
 * `gcc blend.c include/glad.c -lglfw -lil -framework Cocoa -framework OpenGL -framework IOKit`
 *
 * For Glad installation, see README.md
 */

#include <stdio.h>
#include <stdlib.h>
#include <IL/il.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

const unsigned int VIEWPORT_WIDTH = 960;
const unsigned int VIEWPORT_HEIGHT = 540;

unsigned int load_image(const char *filepath) {
    // Load an image at `filepath` and return its DevIL id.
    ILuint image_id;
    printf("Loading image: %s\n", filepath);
    ilGenImages(1, &image_id);
    ilBindImage(image_id);
    ilEnable(IL_ORIGIN_SET);
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
    ilLoadImage(filepath);
    // QUESTION: Does this do unnecessary work if the image is already RGB?
    ilConvertImage(IL_RGB, IL_UNSIGNED_BYTE);
    printf("Width: %d, Height %d, Bytes per Pixel %d\n",
        ilGetInteger(IL_IMAGE_WIDTH),
        ilGetInteger(IL_IMAGE_HEIGHT),
        ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL));
    return image_id;
}

void write_image(char *filepath, int width, int height, void *buffer) {
    // Save an image to disk at `filepath` given image data in `buffer`.
    ILuint image;
    int pixel_size = 1 * 3; // 3 bytes for RGB
    ilGenImages(1, &image);
    ilBindImage(image);
    ilTexImage(width, height, 0, pixel_size, IL_RGB, IL_UNSIGNED_BYTE, buffer);
    ilSaveImage(filepath);
    ilDeleteImage(image);
}

int main() {
    // Vertex shader source
    const char *vertex_shader_source =
        "#version 410\n"
        "layout (location = 0) in vec3 aPos;"
        "layout (location = 1) in vec3 aColor;"
        "layout (location = 2) in vec2 aTexCoord;"
        "out vec3 ourColor;"
        "out vec2 TexCoord;"
        "void main () {"
        "  gl_Position = vec4(aPos, 1.0);"
        "  ourColor = aColor;"
        "  TexCoord = aTexCoord;"
        // "  TexCoord = vec2(aTexCoord.x, aTexCoord.y);"
        "}";

    // Fragment shader source
    const char *fragment_shader_source =
        "#version 410\n"
        "out vec4 FragColor;"
        "in vec3 ourColor;"
        "in vec2 TexCoord;"
        "uniform sampler2D texture1;"
        "uniform sampler2D texture2;"
        "void main () {"
        // "  FragColor = vec4(0.5, 0.0, 0.5, 1.0);"
        // "  FragColor = texture(texture1, TexCoord);"
        // "  FragColor = mix(texture(texture1, TexCoord), texture(texture2, TexCoord), 0.5);"
        "FragColor = min(texture(texture1, TexCoord), texture(texture2, TexCoord));"
        // "FragColor = vec4(ourColor, 1.0);"
        "}";

    // Shader IDs
    GLuint vertex_shader_id, fragment_shader_id, shader_program_id;

    // Init DevIL
    ilInit();

    // Init and configure GLFW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // GLFW window creation
    GLFWwindow* window = glfwCreateWindow(
        VIEWPORT_WIDTH, VIEWPORT_HEIGHT,
        "My First OpenGL Program",
        NULL, NULL);
    if (window == NULL) {
        printf("Failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Load OpenGL function pointers via Glad
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        printf("Failed to initialize GLAD\n");
        return -1;
    }

    // Define vertices to draw a quad from two triangles to display textures
    float vertices[] = {
        // positions          // colors           // texture coords
         1.0f,  1.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
         1.0f, -1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        -1.0f, -1.0f, 0.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, // bottom left
        -1.0f,  1.0f, 0.0f,   1.0f, 1.0f, 0.0f,   0.0f, 1.0f  // top left
    };
    unsigned int indices[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };
    // Vertex arrays, element arrays
    unsigned int VBO, VAO, EBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // Position coordinates
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // RGB Color
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    // Texture coordinates
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);

    // Prepare to load textures
    unsigned int texture1, texture2;
    int width, height;
    unsigned char* data;
    int id;

    // Texture 1
    glGenTextures(1, &texture1);
    glBindTexture(GL_TEXTURE_2D, texture1);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    id = load_image("input/a.png");
    ilBindImage(id);
    width = ilGetInteger(IL_IMAGE_WIDTH);
    height = ilGetInteger(IL_IMAGE_HEIGHT);
    data = ilGetData();
    printf("Preparing texture1 with width: %d, height: %d\n", width, height);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                width, height,
                0, GL_RGB, GL_UNSIGNED_BYTE,
                data);

    // Texture 2
    glGenTextures(1, &texture2);
    glBindTexture(GL_TEXTURE_2D, texture2);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    id = load_image("input/b.png");
    ilBindImage(id);
    width = ilGetInteger(IL_IMAGE_WIDTH);
    height = ilGetInteger(IL_IMAGE_HEIGHT);
    data = ilGetData();
    printf("Preparing texture2 with width: %d, height: %d\n", width, height);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
                  width, height,
                  0, GL_RGB, GL_UNSIGNED_BYTE,
                  data);

    // Build the shader program
    vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader_id, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader_id);
    fragment_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader_id, 1, &fragment_shader_source, NULL);
    glCompileShader(fragment_shader_id);
    shader_program_id = glCreateProgram();
    glAttachShader(shader_program_id, vertex_shader_id);
    glAttachShader(shader_program_id, fragment_shader_id);
    glLinkProgram(shader_program_id);

    // Activate the shader before setting uniforms
    glUseProgram(shader_program_id);

    // Set which texture units belong to which samplers
    glUniform1i(glGetUniformLocation(shader_program_id, "texture1"), 0);
    glUniform1i(glGetUniformLocation(shader_program_id, "texture2"), 1);

    // Render loop
    while (!glfwWindowShouldClose(window)) {
        glClear(GL_COLOR_BUFFER_BIT);

        // Bind textures to texture units
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture1);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture2);

        // Activate the shader program
        glUseProgram(shader_program_id);

        // Bind the vertices and draw the elements
        glBindVertexArray(VAO);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        // Save the resulting image
        int fb_width, fb_height;
        glfwGetFramebufferSize(window, &fb_width, &fb_height);
        printf("Framebuffer width: %d, height: %d\n", fb_width, fb_height);
        void *output;
        output = malloc(fb_width * fb_height * 3);
        printf("Saving image with width: %d, height: %d\n", fb_width, fb_height);
        glReadPixels(0, 0, fb_width, fb_height, GL_RGB, GL_UNSIGNED_BYTE, output);
        write_image("output/c-gpu.png", fb_width, fb_height, output);
        break;

        // Swap buffers and poll events via GLFW
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // Deallocate resources
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteBuffers(1, &EBO);

    // Terminate GLFW
    glfwTerminate();
    return 0;
}
