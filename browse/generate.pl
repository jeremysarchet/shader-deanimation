#!/usr/bin/perl -l
use strict;
use warnings;

use File::Find;
use File::Basename;

my $dir = '/Volumes/miro/Dropbox/deanimation/raw';

my @year_dirs = glob("$dir/*");

foreach my $year_dir (@year_dirs) {
  print $year_dir;
  my $year = basename($year_dir);
  my $html_filename = "$year.html";

  my @work_dirs = glob("$year_dir/*");
  my $num_works = @work_dirs;

  open(FH, '>', $html_filename) or die $!;

  print FH '<!DOCTYPE html>
<html>
<head>
  <title>Deanimation previews</title>
  <link rel="stylesheet" href="previews.css">
</head>
<body>';

  print FH "  <header>
    <h1>$year</h1>
    <p>Preview stills - $num_works items</p>
  </header>";

  my $first_date = substr(basename($work_dirs[0]), 0, 10);
  print FH "<h2>$first_date</h2>";
  my $previous_date = $first_date;

  foreach my $work_dir (@work_dirs) {
    my @light_preview_thumbnail_matches = glob("$work_dir/*-lighter-color-preview-thumbnail.jpg");
    my $light_preview_thumbnail = $light_preview_thumbnail_matches[0] // '';
    my $light_preview = $light_preview_thumbnail;
    $light_preview =~ s/-thumbnail//;

    my @dark_preview_thumbnail_matches = glob("$work_dir/*-darker-color-preview-thumbnail.jpg");
    my $dark_preview_thumbnail = $dark_preview_thumbnail_matches[0] // '';
    my $dark_preview = $dark_preview_thumbnail;
    $dark_preview =~ s/-thumbnail//;

    my $identifier = basename($work_dir);
    my $date = substr($identifier, 0, 10);

    if ($date ne $previous_date) {
        print FH "<h2>$date</h2>";
    }

    print FH '  <div class="work">';
    print FH "    <a href='$light_preview'><img src='$light_preview_thumbnail' /></a>";
    print FH "    <a href='$dark_preview'><img src='$dark_preview_thumbnail' /></a>";
    print FH "    <p>$identifier</p>";
    print FH '  </div>
    ';

    $previous_date = $date;
  }

  print FH '</body>
  </html>';
  close FH;
}

