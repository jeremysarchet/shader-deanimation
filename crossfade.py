"""Concatenate a series of videos via crossfade.

Usage: `python3 crossfade.py <output-path> <crossfade-duration> <input-paths>`

E.g. `python3 crossfade.py out.mp4 1.0 a.mp4 b.mp4 c.mp4 d.mp4`
"""
import sys
import ffmpeg

# Ensure at least enough arguments are provided to define crossfading two videos
if len(sys.argv) < 5:
    print("Not enough arguments provided.")
    exit(1)

# Assign and prepare command-line arguments
output_path = sys.argv[1]
crossfade_duration = float(sys.argv[2])
input_paths = sys.argv[3:]
print("Output path: ", output_path)
print("Crossfade duration: ", crossfade_duration)
print("Input paths: ", input_paths)


def get_duration(video_path):
    """Returns the duration of the video at `video_path` in seconds

    https://github.com/kkroening/ffmpeg-python/tree/master/examples#get-video-info-ffprobe
    """
    probe = ffmpeg.probe(video_path)
    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    return float(video_stream['duration'])

# For the life of me, can't figure out why, if the split point is such that the
# `zeroth_head` is longer than the crossfade duration, that part that should end
# up as-is following the final crossfade doesn't go through. It seems the
# `xfade` when used this way at the end with an input that has already been
# `xfade`d, results in the final output video ending right when the crossfade
# is complete. See `basic-xfade-text.sh` for minimal reproduction. Incidentally,
# for my needs, it's perfectly adequate to split the zeroth video at a point
# equal to the crossfade duration, so solving this issue isn't of practical
# importance.
SPLIT_POINT = crossfade_duration

# Go through and determine when each successive video should start
start_times = [0.0]
t = 0  # Initial offset is 0
for i in range(len(input_paths) + 1):
    if i == 0:
        continue
    elif i == 1:
        t = get_duration(input_paths[0]) - SPLIT_POINT - crossfade_duration
    else:
        # The offset for a given video relative to the preceding offset is
        # the duration of the previous video minus the crossfade
        t += get_duration(input_paths[i - 1]) - crossfade_duration
    start_times.append(t)

print("Start times: ", start_times)

# Construct an array of inputs for each path
ffmpeg_inputs = [ffmpeg.input(path) for path in input_paths]

# For seamless loop, split the zeroth input, as we'll work with it twice in
# the filter complex.
zeroth = ffmpeg_inputs[0].split()

# The head starts at 0 seconds and goes for N seconds duratino
zeroth_head = zeroth[1].trim(duration=SPLIT_POINT)

# The tail starts at the N second mark and goes to the end
zeroth_tail = zeroth[0].trim(start=SPLIT_POINT).setpts("PTS-STARTPTS") # Got to set the timestamps to zero when taking the tail

# For seamless loop, the initial input zeroth input starting at N seconds
result = zeroth_tail

# Construct a filter complex, chaining crossfades for each successive video
for i in range(1, len(input_paths)):
    result = ffmpeg.filter(
        [result, ffmpeg_inputs[i]],
        'xfade',
        duration=crossfade_duration,
        offset=start_times[i]
    )

# For seamless loop, the final input is the first N seconds of the 0th video
result = ffmpeg.filter(
    [result, zeroth_head],
    'xfade',
    duration=crossfade_duration,
    offset=start_times[-1]
)

# Prepare and render the output
out = ffmpeg.output(result, output_path, pix_fmt='yuv420p')
print("FFMPEG args:")
for arg in out.get_args():
    if ";" in arg:
        for segment in arg.split(";"):
            print(segment + ";")
    else:
        print(arg)
print("~" * 80)
out.run()
