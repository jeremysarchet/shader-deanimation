#!/bin/bash
# Shell script for processing deanimation videos

# Updgrde to yq version 4.
# This script was originally written to check for variables being falsy
# if a certain value isn't specified in the yaml file.
# Now, with yq version 4,  when a value doesn't exist it returns the string
# 'null'. So we can pass the `-e` flag to get it to error out in this case.
# But this comes with the downside that it logs an error message, sigh. An
# alternative would be:
#
# if [[ $foo == 'null' ]] ; then unset foo ; fi
#
# But this is unsatisfying

# Set log level for FFMPEG
loglevel=info

# Ensure exactly two arguments supplied
if [[ $# -ne 1 ]] ; then
    echo 'Usage: ./process.sh <blend id>'
    exit 1
fi

# Receive command line arguments as inputs
blend_id=$1

# Video ID is first 17 characters of blend id
# E.g. 2020-06-27-111713-1 (<video ID>-<blend spec>)
video_id="${blend_id:0:17}"
echo "Blend ID: $blend_id"
echo "Video ID: $video_id"

RECIPES_FILE='works-2.yml'

year="${blend_id:0:4}"
current_date=$(date +"%Y-%m-%d")

# Ensure there's an entry for the given video ID
if [[ $(yq e ".${blend_id}" $RECIPES_FILE) == 'null' ]] ; then
    echo "No work found for the given blend ID."
    exit 1
fi

# Read in global configuration parameters
raw_video_dir=$(yq e ".raw_video_dir" global.yml)/${year}/${video_id}
preprocessed_video_dir=$(yq e ".preprocessed_video_dir" global.yml)/${year}
scratch_dir=$(yq e ".scratch_dir" global.yml)/${year}
processed_dir=$(yq e ".processed_dir" global.yml)/${year}
frame_basename_pattern=$(yq e ".frame_basename_pattern" global.yml)
frame_file_extension=$(yq e ".frame_file_extension" global.yml)
frame_pattern=$frame_basename_pattern.$frame_file_extension

# Video path
video_filename=$(yq e ".${blend_id}.extract.raw" $RECIPES_FILE -e 2> /dev/null) || unset video_filename
if [[ ! $video_filename ]]; then
  echo "Video filename not provided in recipe (expected to find at `extract.raw`)"
  exit 1
fi
video_path=${raw_video_dir}/${video_filename}
echo "Video path:    ${video_path}"

# Ensure the video exists
if [[ ! -f $video_path ]]; then
    echo "Raw video not round."
    exit 1
fi

# Frame count
# https://stackoverflow.com/questions/2017843/fetch-frame-count-with-ffmpeg
printf "Source frames:   "
source_frames=$(ffprobe \
    -loglevel error \
    -select_streams v:0 \
    -show_entries stream=nb_frames \
    -of default=nokey=1:noprint_wrappers=1 \
    $video_path)
echo $source_frames

# Frame rate
printf "Source frame rate: "
frame_rate=$(ffprobe \
    -loglevel error \
    -select_streams v:0 \
    -show_entries stream=r_frame_rate \
    -of default=nokey=1:noprint_wrappers=1 \
    $video_path)
echo $frame_rate

# Dimensions
# https://stackoverflow.com/questions/7362130/getting-video-dimension-resolution-width-x-height-from-ffmpeg
printf "Dimensions:    "
dimensions=$(ffprobe \
    -loglevel error \
    -show_entries stream=width,height \
    -of csv=p=0:s=x \
    $video_path)
dimensions=$(sed 's/x/:/' <<< $dimensions)
echo $dimensions


# Read extraction parameters
mod=$(yq e ".${blend_id}.extract.mod // 1" $RECIPES_FILE)
seek=$(yq e ".${blend_id}.extract.seek // 0" $RECIPES_FILE)
extraction_frames=$(yq e ".${blend_id}.extract.frames // $source_frames" $RECIPES_FILE)
scale=$(yq e ".${blend_id}.extract.scale" $RECIPES_FILE -e 2> /dev/null) || scale=$dimensions
crop=$(yq e ".${blend_id}.extract.crop" $RECIPES_FILE -e 2> /dev/null) || crop=$dimensions
rotate=$(yq e ".${blend_id}.extract.rotate // 0" $RECIPES_FILE)
sharpen=$(yq e ".${blend_id}.extract.sharpen" $RECIPES_FILE -e 2> /dev/null) || unset sharpen
crossfades=$(yq e ".${blend_id}.extract.crossfades" $RECIPES_FILE -e 2> /dev/null) || unset crossfades
master=$(yq e ".${blend_id}.extract.curves.master" $RECIPES_FILE -e 2> /dev/null) || unset master
red=$(yq e ".${blend_id}.extract.curves.red" $RECIPES_FILE -e 2> /dev/null) || unset red
green=$(yq e ".${blend_id}.extract.curves.green" $RECIPES_FILE -e 2> /dev/null) || unset green
blue=$(yq e ".${blend_id}.extract.curves.blue" $RECIPES_FILE -e 2> /dev/null) || unset blue
curves="master='${master}':red='${red}':green='${green}':blue='${blue}'"
original_poster=$(yq e ".${blend_id}.extract.poster // 0" $RECIPES_FILE -e 2> /dev/null) || unset original_poster
preprocessed_path=$(yq e ".${blend_id}.extract.preprocessed" $RECIPES_FILE -e 2> /dev/null) || unset preprocessed_path
keeper_intervals=$(yq e ".${blend_id}.extract.intervals" $RECIPES_FILE -e 2> /dev/null) || unset keeper_intervals
IFS=', ' read -r -a intervals <<< $keeper_intervals

# Setup one or more possible filters for selecting frames
select=""
# Setup a not mod statement for the select filter if need be
if [[ $mod && $mod -ne 1 ]] ; then
    select+="not(mod(n\,${mod}))"
fi

# Setup a between statements for intervals
if [[ $intervals ]] ; then
    if [[ $select ]] ; then
        select+="*"
    fi
    select+="("
    # Go through each interval and split on dash token
    for i in "${intervals[@]}"
    do
        IFS='-' read start end <<< $i
        start=${i%-*}
        end=${i##*-}
        select+="between(n\,${start}\,${end})+"
    done
    # Get rid of final `+`
    select=$(sed 's/.$//' <<< $select)
    select+=")"
fi

# Read deanimation parameters
mode=$(yq e ".${blend_id}.deanimate.mode" $RECIPES_FILE)
step=$(yq e ".${blend_id}.deanimate.step" $RECIPES_FILE)
impressions=$(yq e ".${blend_id}.deanimate.impressions" $RECIPES_FILE)
span=$((step * impressions))
result_poster=$(yq e ".${blend_id}.deanimate.poster // $span" $RECIPES_FILE)

# Alter input path if preprocessed video path provided
if [[ $preprocessed_path ]] ; then
    video_path="${preprocessed_video_dir}/${preprocessed_path}"
fi

# Prepare names for directories and files
suffix="m${mod}-s${step}-i${impressions}"
work_dir="${scratch_dir}/${blend_id}"
output_dir="${processed_dir}/${blend_id}"

echo
echo "Work dir: $work_dir"
mkdir $work_dir
echo "Output dir: $output_dir"
mkdir $output_dir
echo

extraction_dir="${work_dir}/01-extracted-m${mod}"
deanimation_dir="${work_dir}/02-deanimated-${suffix}"


# Ensure reference image exists if mode is "further-from"
if [[ $mode == 'further-from' ]] ; then
    reference_image="${output_dir}/median-background.png"
    if [[ ! -f $reference_image ]] ; then
        echo "Couldn't find reference image: ${reference_image}"
        exit 1
    fi
fi

# Duration in seconds
# https://stackoverflow.com/a/24488789
printf "Duration (s):  "
duration=$(ffprobe \
    -loglevel error \
    -print_format compact=print_section=0:nokey=1:escape=csv \
    -show_entries format=duration \
    $video_path)
echo $duration


function raw_preview() {
    # Raw preview
    printf "\n--------------Raw preview--------------\n"
    ffmpeg \
        -i $video_path \
        -c:v h264_videotoolbox \
        -b:v 8m \
        -pix_fmt yuv420p \
        -vf "scale=1920x1080" \
        -loglevel $loglevel \
        -hide_banner \
        "${output_dir}/${blend_id}_${current_date}-raw-preview.mp4"
}

function make_crossfades() {
  pre_crossfades_dir="${work_dir}/01-extracted-m${mod}-pre-crossfades/"

  # TODO: Don't do it this way. Make an official crossfade dir instead.
  rm -r $pre_crossfades_dir # Overwrite
  mv $extraction_dir $pre_crossfades_dir  # Rename
  mkdir -p $extraction_dir  # New, empty extraction dir

  python3 utilities/crossfade.py $pre_crossfades_dir $extraction_dir "${keeper_intervals}"
}

function extract() {
    # Extract
    printf "\n--------------Extracting--------------\n"
    echo "Extraction frames: ${extraction_frames}"
    echo "Select: ${select}"
    echo "Curves: ${curves}"
    echo "Seek: ${seek}"
    echo "Scale: ${scale}"
    echo "Crop: ${crop}"
    echo "Rotate: ${rotate}"
    filter="curves=${curves}, scale=${scale}, crop=${crop}, rotate=${rotate}"
    if [[ $sharpen ]] ; then
        filter+=", unsharp"
    fi
    if [[ ! $crossfades ]]; then
      filter+=", select=${select}"
    fi
    echo "Filter: ${filter}"

    rm -r $extraction_dir; mkdir -p $extraction_dir  # Overwrite
    ffmpeg \
        -ss $seek \
        -i $video_path \
        -frames:v $extraction_frames \
        -vf "${filter}" \
        -start_number 0 \
        ${extraction_dir}/${frame_pattern} \
        -vsync "vfr" \
        -loglevel $loglevel \
        -hide_banner

    if [[ $crossfades ]]; then
      make_crossfades
    fi
}

function encode_original() {
    # Encode original
    printf "\n--------------Encoding original--------------\n"
    echo "Input dir: ${extraction_dir}"
    echo "Output dir: ${output_dir}"
    # At one time doing `-vf zscale=matrix=709` was necessary to fix a color
    # discrepancy between the output video not having same color as the still
    # images.  Now, it seems that causes the error "Could not get pixel format
    # for color format 'yuv420p' range 'pc'.". So, I've removed it. Keep an eye
    # out for color discrepancies. Possibly, avoiding GPU acceleration for
    # encoding is the thing to do to avoid such troubles.
    # See: trac.ffmpeg.org/ticket/8056
    ffmpeg \
        -r 24 \
        -i ${extraction_dir}/${frame_pattern} \
        -c:v h264_videotoolbox \
        -b:v 8m \
        -pix_fmt yuv420p \
        -loglevel $loglevel \
        -hide_banner \
        "${output_dir}/${blend_id}_${current_date}-m${mod}-original.mp4"
    # Poster frame
    convert \
        "${extraction_dir}/${original_poster}.bmp" \
        "${output_dir}/${blend_id}_${current_date}-m${mod}-original-poster.png"
}

function deanimate() {
    # Count the number of frames to be deanimated
    deanimation_frames=$(find ${extraction_dir} -name "*.bmp" | wc -l)
    # Deanimate
    printf "\n--------------Deanimating--------------\n"
    echo "Frame pattern:      ${frame_pattern}"
    echo "Deanimation frames: ${deanimation_frames}"
    echo "Mode:               ${mode}"
    echo "Step:               ${step}"
    echo "Impressions:        ${impressions}"
    if [ -d "$deanimation_dir" ]; then
      rm -r $deanimation_dir
      echo
      echo "Directory $deanimation_dir exists. Removing.\n"
    fi
    mkdir -p $deanimation_dir

    additional_options=''
    if [[ $reference_image ]]; then
        additional_options+=" -p $reference_image"
    fi
    ./tail_sequence \
        -i $extraction_dir \
        -o $deanimation_dir \
        -l $deanimation_frames \
        -m 'tail-sequence' \
        -b $mode \
        -s $step \
        -t $impressions \
        $additional_options
}

function encode_result() {
    bitrate=$1 # Mbps
    # Note: removed `-vf zscale=matrix=709`. See comment above in
    # `encode_original` function
    # Note: the CRF doesn't seem to be applicable to GPU-accelerated encoding
    # Working with the bitrate parameter instead.
    printf "\n--------------Encoding result--------------\n"
    echo "bitrate: $bitrate"
    ffmpeg \
        -r 24 \
        -start_number  $span \
        -i ${deanimation_dir}/${frame_pattern} \
        -c:v h264_videotoolbox \
        -b:v "${bitrate}M" \
        -pix_fmt yuv420p \
        -loglevel $loglevel \
        -hide_banner \
        "${output_dir}/${blend_id}_${current_date}-${suffix}-${bitrate}Mbps.mp4"
    # Poster frame
    convert \
        "${deanimation_dir}/${result_poster}.bmp" \
        "${output_dir}/${blend_id}_${current_date}-${suffix}-poster.png"
}

function seamless_loop() {
    printf "\n--------------Encoding seamless loop--------------\n"
    deanimation_frames=$(find ${extraction_dir} -name "*.bmp" | wc -l)
    main_duration=$(bc <<< "scale=2; ${deanimation_frames} / 24.0 - 2.0")
    echo "Main duration: ${main_duration}"
    ffmpeg \
        -r 24 \
        -start_number  $span \
        -i ${deanimation_dir}/${frame_pattern} \
        -pix_fmt yuv420p \
        -filter_complex "[0]split[body][pre]; \
            [pre]trim=duration=1,format=yuv420p,fade=d=1:alpha=1,setpts=PTS+(${main_duration}/TB)[jt]; \
            [body]trim=1,setpts=PTS-STARTPTS[main]; \
            [main][jt]overlay" \
        "${output_dir}/${blend_id}_${current_date}-${suffix}-loopable.mp4"
}
# TODO: Generate and save a text file to the output dir which records
# information about the blend.

# Run processing steps
raw_preview
extract
encode_original
deanimate
encode_result 8 # default
#encode_result 4 # lower quality
#seamless_loop
