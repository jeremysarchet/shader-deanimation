# Concatenate a series of videos with crossfades


function get_duration() {
  # Expects $1 to be path to video
  # https://stackoverflow.com/a/24488789
  ffprobe \
    -v quiet \
    -print_format compact=print_section=0:nokey=1:escape=csv \
    -show_entries format=duration \
    $1
}

crossfade_duration=1.0

vid_0=concat-test/0.mp4
vid_1=concat-test/1.mp4
vid_2=concat-test/2.mp4
vid_3=concat-test/3.mp4
vid_4=concat-test/4.mp4

vid_0_duration=$(get_duration $vid_0)
vid_1_duration=$(get_duration $vid_1)
vid_2_duration=$(get_duration $vid_2)
vid_3_duration=$(get_duration $vid_3)
vid_4_duration=$(get_duration $vid_4)

vid_0_start=0
vid_1_start=$(bc <<< "${vid_0_start} + ${vid_0_duration} - ${crossfade_duration}")
vid_2_start=$(bc <<< "${vid_1_start} + ${vid_1_duration} - ${crossfade_duration}")
vid_3_start=$(bc <<< "${vid_2_start} + ${vid_2_duration} - ${crossfade_duration}")
vid_4_start=$(bc <<< "${vid_3_start} + ${vid_3_duration} - ${crossfade_duration}")

# https://stackoverflow.com/a/37812010
# https://trac.ffmpeg.org/wiki/Xfade
filter_complex="
  [0][1] xfade=duration=${crossfade_duration}:offset=${vid_1_start} [a];
  [a][2] xfade=duration=${crossfade_duration}:offset=${vid_2_start} [b];
  [b][3] xfade=duration=${crossfade_duration}:offset=${vid_3_start} [c];
  [c][4] xfade=duration=${crossfade_duration}:offset=${vid_4_start}
"

echo $filter_complex

ffmpeg \
-i $vid_0 \
-i $vid_1 \
-i $vid_2 \
-i $vid_3 \
-i $vid_4 \
-filter_complex "${filter_complex}" \
-pix_fmt yuv420p \
out.mp4

open -a "QuickTime Player" out.mp4
