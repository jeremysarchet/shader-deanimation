# Shader Deanimation
Exploring how I might do image processing on the GPU.

# Setup
Compiling the OpenGL program, `tail_sequence`

## Install GLFW
`brew install glfw`

## Install DevIL
`brew install devil`

## Instal GLAD
Glad project: https://github.com/Dav1dde/glad

Here's what I've done on macOS:

- Use the glad web service: https://glad.dav1d.de/
- Language: "C/C++"
- Specification: "OpenGL"
- API gl "Version 4.1"
    - Recently got it going with 4.6, btw
- Profile: "Compatibility"
- Then, for extensions, click the "Add all" button
- Ensure "Generate a loader" is checked
- Click "Generate"
- Download `glad.zip`
- Extract glad.zip
- Copy the `glad` and `KHR` folders to `/usr/local/include`
- Copy `glad.c` to `include/glad.c`

### Library search paths
On an M1 mac, it seems `brew` doesn't put things in `/usr/local/include`.
I've added the following pair of environment variables like so from the
following post:
```
export CPATH=/opt/homebrew/include
export LIBRARY_PATH=/opt/homebrew/lib
```
https://stackoverflow.com/a/67378304

## Compile
`gcc sequence_blend.c include/glad.c -lGLFW -lIL -framework Cocoa -framework OpenGL -framework IOKit -o tail_sequence`

### Compatibility vs Core profiles
The first time I got this going, "Compatibility profile" naively sounded like
a wiser choice for building this program.

When running this way, the program sets up an OpenGL 2.1 context. Otherwise, on
the two macOS machines I've used, it sets up an OpenGL 4.1 context.

I didn't realize at the time that my wanting to use such features as
framebuffer objects, meant I needed extensions on top of what 2.1 offers.

Sure enough, telling GLAD to give me "all the extensions" got my program
going just fine.

It feels like it kind of defeats the purpose of "Compatibility profile" if I'm
going to then go and add all the extensions. Perhaps I should just admit
I'm using more modern features than OpenGL 2.1 intrinsically contains,
(such as framebuffer objects), and concede that the upstanding thing to do
would be to build this with OpenGL 4.1.

Abstractly, it would feel nice to be able to run this with a more basic and
primitive set of functionality. But having to bring in extensions makes things
feel hypocritical.

If I were to switch to 4.1, I would need to port the existing shader code to
`#version 410` or so.

# Benchmarking
Here's a minimal test I've used to time the end to end process:

```
time ( \
ffmpeg -i 'test/in.mp4' test/extracted/%d.bmp && \
./deanimate -i test/extracted/ -o test/blended/ -l 120 -m 'tail-sequence' -b 'lighter-color'
ffmpeg -start_number 10 -i test/blended/%d.bmp -c:v h264_videotoolbox -b:v 5000k -pix_fmt yuv420p test/out-gpu.mp4
)
```

# Development resources
- https://giovanni.codes/opengl-setup-in-macos/
- http://duriansoftware.com/joe/An-intro-to-modern-OpenGL.-Table-of-Contents.html
- https://learnopengl.com
- http://learnopengl.com/Getting-started/Creating-a-window
- https://open.gl
- http://docs.gl
- https://antongerdelan.net/opengl/
  - https://github.com/capnramses/antons_opengl_tutorials_book
  - https://antongerdelan.net/teaching/3dprog1/maths_cheat_sheet.pdf
- https://www.khronos.org/registry/OpenGL/index_gl.php
- OpenGL Extensions Viewer from the macOS app store

## Relevant tutorials
- https://webglfundamentals.org/webgl/lessons/webgl-2-textures.html
- https://webglfundamentals.org/webgl/lessons/webgl-image-processing.html

## Look into:
- https://github.com/polyfloyd/shady
- https://github.com/ReubenJCarter/GPU-Programming
- https://colab.research.google.com/drive/1eXsoH1LOlBxtI3j8Beaj3r1t7HaqXiR2?usp=sharing
- https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/By_example/Hello_GLSL
- https://github.com/patriciogonzalezvivo/glslCanvas
- http://bantherewind.com/wrap-your-mind-around-your-gpu

## Notes to self:
- The OpenGL header files are found here on my mac:
  - `/opt/X11/include, e.g. /opt/X11/include/GL/gl.h.`
- The OpenGL framework is here:
  - `/System/Library/Frameworks`
- The program `glxinfo` may provide relevant information.
  - E.g. to find version information: `glxinfo | grep version`

## Further resources
- https://opengl.org/sdk/docs/tutorials/
- https://www.opengl.org/sdk/docs/tutorials/TyphoonLabs/Chapter_1.pdf
- http://www.lighthouse3d.com/tutorials/glsl-tutorial
- https://www2.cs.duke.edu/courses/compsci344/spring15/classwork/15_shading/glsl_cornell.pdf
- https://stackoverflow.com/questions/2795044/easy-framework-for-opengl-shaders-in-c-c
- https://github.com/BradLarson/GPUImage
- https://developers.google.com/web/fundamentals/media/manipulating/live-effects
- http://docs.gl/sl4/max
